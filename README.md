# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Exchange Calendar Mirror is an application that silently runs as a service to keep a pair of calendar mirror. The mirroring process takes the source calendar and creates a clone on the target calendar. The appointments on the target calendar will always look the same as the appointments on the source calendar, no more and no less.

Feature:

-Requires only Exchange Webmail to connect

-Multiple source and target mirroring

-Source and target webmail do not have to be on the same domain

-Concurrent mirroring

-Runs silently 24/7 as a service

-Easy install and uninstall service

-Adjustable time frame to observe changes

-Adjustable replication intervals

-Auto resume from disconnection

-Account validation

-Logs all changes in CVS

-Limit logs (optional)

Features (Exchange Calendar Spammer - use for stress testing):

-Auto resume from disconnection

-Account validation

-Custom: Subject, Body, Location fields

-Random date range to spam

-Random color category

-Random delete

-Spam tracking

-Creates somewhat large appointment entries with randomized text

-Quickly spam the calendar once in one click

-Spam by: total duration, concurrent queues, delays, Quick Spam, Auto Spam

-Does not break the calendar when enabling Random delete or using Auto Spam

![Scheme](/0.JPG)
![Scheme](/1.jpg)

### How do I get set up? ###

Compiled version in my Google drive. It's a plain exe application file, but has a built-in service installer.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact