﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Topshelf;
using CalendarMirror;

namespace CalendarMirrorService
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x => //1
            {

                x.Service<ServiceMode>(s => //2
                {
                    s.ConstructUsing(name => new ServiceMode()); //3
                    s.WhenStarted(m => m.Start()); //4    
                    s.WhenStopped(m => m.Stop());
                    x.RunAsLocalSystem(); //6

                    x.SetDescription("Mirrors Exchange Calendars"); //7
                    x.SetDisplayName("Calendar Mirror"); //8
                    x.SetServiceName("Calendar Mirror"); //9
                });
            });
        }
    }

    class ServiceMode
    {
        private MirrorAll Job;
        public ServiceMode()
        {
            
        }

        public void Start()
        {
            Job = new MirrorAll();
            Job.Start();
            //while (Job.MainRunning)
            //{
            //    Thread.Sleep(5000);
            //}
        }

        public void Stop()
        {
            Job.Stop();
        }
    }
}


