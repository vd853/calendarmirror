﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using CalendarMirror;
using Utilities;
using Microsoft.Exchange.WebServices.Data;
using Task = System.Threading.Tasks.Task;

namespace CalendarSpammer
{
    //public string Subject { get; set; }
    //public DateTime End { get; set; }
    //public DateTime Start { get; set; }
    //public StringList Catagories { get; set; }
    //public Importance Importance { get; set; }
    //public string Location { get; set; }

    ////bottom properties not use for comparison
    //public MessageBody body { get; set; }

    //public ItemId ID { get; set; } //not use for creating appointments

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Utilities.FileSerialization Data;
        public ConfigModel Config { get; set; }
        private bool _dataLoaded;
        private Task spammer;
        private bool Spamming;
        private string AlertMessage;
        public int iter;
        private const int iterGCLimit = 1000;
        public int iterGC; //use for GC
        public bool isAdded; //use for regulated
        private ExchangeService Service;
        CancellationTokenSource tokenSource;
        private const int PurgeLimit = 200; //if this much entry exist, DeleteAll everything
        public MainWindow()
        {
            InitializeComponent();
            //Alert.Visibility = Visibility.Hidden;
            Fields_SP.IsEnabled = false;
            Spam_BT.IsEnabled = false;
            var possibleDefault = System.IO.Directory.GetCurrentDirectory() + "\\" + "data.ecspam";
            var path = File.Exists(possibleDefault);
            if (path)
            {
                Data = new FileSerialization(possibleDefault, ConfigModel.DefaultConfig(), false);
                load();
            }
            else
            {
                Alert.Content = "Ready";
            }  
            Spam_BT.Content = "Start Now";
        }
        private void New_BT_Click(object sender, RoutedEventArgs e)
        {
            New();
        }
        private void Load_BT_Click(object sender, RoutedEventArgs e)
        {
            InitLoad();
        }
        
        void New()
        {
            Data = new FileSerialization("data.ecspam", ConfigModel.DefaultConfig());
            load();
            DataToControls();
            _dataLoaded = true;
            SpamControls(true);
        }
        void InitLoad()
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".ecspam";
            dlg.Filter = "ECSpam Files (*.ecspam)|*.ecspam";


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document  
                _dataLoaded = false;
                data_TB.Text = dlg.FileName;
                Data = new FileSerialization(dlg.FileName, ConfigModel.DefaultConfig(), false);
                load();
            }  
        }
        private async void Spam_BT_Click(object sender, RoutedEventArgs e)
        {
            save();
            var limits = Config.Concurrent;
            if (limits == 0)
            {
                limits = 1;
            }
            var KeepGoing = true; //internal cancellation
            var Stopwatch = new Stopwatch();
            iter = 0;
            if (Spamming)
            { 
                cancellations();
                await spammer;
                Spam_BT.IsEnabled = true;
                SpamControls(true);
            }
            else
            {
                SpamControls(false);
                Spamming = true;
                Spam_BT.Content = "Stop Spam";
                Stopwatch.Start();
                spammer = Task.Run(() =>
                {
                    var haltNewJobs = false;
                    var jobs = new List<Task<bool>>();
                    tokenSource = new CancellationTokenSource();
                    while (Spamming)
                    {
                        var swSeconds = (double)Stopwatch.ElapsedMilliseconds / 1000;
                        Console.WriteLine("SPAM: " + swSeconds);
                        iter++;
                        var iterHold = iter;

                        if (!haltNewJobs)
                        {
                            var token = tokenSource.Token;
                            Task<bool> t = Task<bool>.Factory.StartNew(() =>
                            {
                                return spam(iterHold);
                            }, token);

                            //Cancellation is use everywhere before or after an exchange is connection is called
                            //if (tokenSource.IsCancellationRequested) return false;

                            //var t = Task.Run(() => spam(iterHold));
                            jobs.Add(t);
                        }
                        
                        //If a connection fails, stop spamming, internal cancelation
                        Task.Run(() =>
                        {
                            try
                            {
                                if (jobs.Exists(x => x.Result == false))
                                {
                                    Thread.Sleep(5000);
                                }
                            }
                            catch
                            {
                                Console.WriteLine("Fail to check false, keep going");
                            }  
                        });

                        //Remove completed jobs
                        var JobsRemove = new List<Task<bool>>();
                        foreach (var t1 in jobs)
                        {
                            if (t1.IsCompleted)
                            {
                                t1.Dispose();
                                JobsRemove.Add(t1);
                            }
                        }
                        foreach (var t2 in JobsRemove)
                        {
                            jobs.Remove(t2);
                        }

                        //wait if there are too many jobs
                        //Console.WriteLine("Job count: " + Jobs.Count);
                        if (!Config.Regulated)
                        {
                            if (jobs.Count > limits)
                            {
                                haltNewJobs = true;
                                Thread.Sleep(5000);
                            }
                            else
                            {
                                haltNewJobs = false;
                            }
                        }

                        //Check if exeeds duration
                        if (Config.Duration != 0 && !Config.Regulated)
                        {  
                            if (swSeconds > Config.Duration*60)
                            {
                                //same code as above without the await and with dispatcher
                                Dispatcher.BeginInvoke(new Action(() =>
                                {
                                    cancellations();
                                    SpamControls(true);
                                }));
                            }
                        }

                        //Global GC then continue
                        if (iterGC > iterGCLimit)
                        {  
                            Dispatcher.BeginInvoke(new Action(() =>
                            {
                                AlertMessage = "GC limit reached";
                                Console.WriteLine(AlertMessage);
                                Dispatcher.BeginInvoke(new Action(() => { Alert.Content = AlertMessage; }));
                            }));
                            Service = Exchange.GetService(Config.Email, Config.Password, Config.Webmail);
                            System.GC.Collect();
                            iterGC = 0;    
                            Thread.Sleep(5000);
                        }
                        else
                        {
                            iterGC++;
                        }

                        #region Regulated
                        if (Config.Regulated)
                        {
                            if (jobs.Count > 100)
                            {
                                haltNewJobs = true;
                                Console.WriteLine("jobs count: " + jobs.Count);
                                Console.WriteLine("iterGC: " + iterGC);
                                Console.WriteLine("Iterations: " + iter);
                                Console.WriteLine("isHaulted: " + haltNewJobs);
                                Thread.Sleep(5000);
                            }
                            else
                            {
                                haltNewJobs = false;
                            }
                        }
                        #endregion

                        #region Cancellation
                        if (!KeepGoing)
                        {
                            Dispatcher.BeginInvoke(new Action(() =>
                            {
                                cancellations();
                                SpamControls(true, true);
                                Alert.Content = "Connection failed, check your settings. You may be locked out.";
                            }));
                        };
                        if (tokenSource.IsCancellationRequested)
                        {
                            break;
                        }
                        #endregion
                        Thread.Sleep(Config.Rate);
                    }
                });
            } 
        }

        #region ControlModifier

        void DataToControls()
        {
            data_TB.Text = Config.DataName;
            MinDays_TB.Text = Config.MinDays.ToString();
            MaxDays_TB.Text = Config.MaxDays.ToString();
            sub_TB.Text = Config.Subject;
            body_TB.Text = Config.Body;
            location_TB.Text = Config.Location;
            rate_TB.Text = Config.Rate.ToString();
            Password.Password = Config.Password;
            WB_TB.Text = Config.Webmail;
            email_TB.Text = Config.Email;
            Delete_CB.IsChecked = Config.Delete;
            cc_TB.Text = Config.Concurrent.ToString();
            duration_TB.Text = Config.Duration.ToString();
            Reg_CB.IsChecked = Config.Regulated;
        }

        void ControlsToData()
        {
            Config.DataName = data_TB.Text;
            Config.MinDays = Utilities.StringFormatter.stringToInt(MinDays_TB.Text);
            Config.MaxDays = Utilities.StringFormatter.stringToInt(MaxDays_TB.Text);
            Config.Subject = sub_TB.Text;
            Config.Body = body_TB.Text;
            Config.Location = location_TB.Text;
            Config.Rate = Utilities.StringFormatter.stringToInt(rate_TB.Text);
            Config.Password = Password.Password;
            Config.Webmail = WB_TB.Text;
            Config.Email = email_TB.Text;
            Config.Delete = Delete_CB.IsChecked.Value;
            Config.Concurrent = Utilities.StringFormatter.stringToInt(cc_TB.Text);
            Config.Duration = Utilities.StringFormatter.stringToInt(duration_TB.Text);
            Config.Regulated = Reg_CB.IsChecked.Value;
        }
        void SpamControls(bool isActive, bool AlertStays = false)
        {
            Quick_BT.IsEnabled = isActive;
            DelCal_BT.IsEnabled = isActive;
            Spam_BT.IsEnabled = true;
            Fields_SP.IsEnabled = isActive;
            New_BT.IsEnabled = isActive;
            Load_BT.IsEnabled = isActive;
            if (!isActive || AlertStays)
            {
                Alert.Visibility = Visibility.Visible;
            }
            else
            {
                Alert.Visibility = Visibility.Hidden;
            }
            RegulatedControls(Config.Regulated);
        }

        void RegulatedControls(bool isRegulated)
        { 
            Delete_CB.IsEnabled = !isRegulated;
            rate_TB.IsEnabled = !isRegulated;
            duration_TB.IsEnabled = !isRegulated;
            cc_TB.IsEnabled = !isRegulated;
        }
        #endregion

        #region Logics

        void cancellations()
        {
            Spamming = false; //external cancellation
            tokenSource.Cancel();
            Spam_BT.Content = "Start Spam";
            Spam_BT.IsEnabled = true;
        }

        private async void Quickie()
        {
            SpamControls(false);
            Spam_BT.IsEnabled = false;
            var limit = 25;
            await Task.Run(() =>
            {
                var T = new List<Task>();
                for (int i = 0; i < limit; i++)
                {
                    var isNow = i;
                    T.Add(Task.Run(() =>
                        {
                            SendRandomAppointment(isNow);
                        })
                    );
                }
                Task.WaitAll(T.ToArray());
            });           
            SpamControls(true);
            Spam_BT.IsEnabled = true;
        }
        bool spam(int iteration)
        {
            //will DeleteAll if reguated, was already added or iteration is even
            var reg = Config.Regulated && isAdded || Config.Regulated && !Utilities.Data.IsOdd(iteration);
            if (Utilities.Data.RandomBool() && Config.Delete || reg)
            {
                isAdded = false;
                if (tokenSource.IsCancellationRequested) return false;
                if (!Exchange.TestService(Config.Email, Config.Password, Config.Webmail))
                {
                    return false;
                }
                //var service = Exchange.GetService(Config.Email, Config.Password, Config.Webmail);
                if (tokenSource.IsCancellationRequested) return false;
                var calenderCompare = CalendarMirror.Comparison.CalendarToComparisonList(Service, 12, 12);

                //no entries or on purge
                var calendarCount = calenderCompare.Count;
                if (calendarCount < 1)
                {
                    AlertMessage = "No calendar entries in random scope";
                    Dispatcher.BeginInvoke(new Action(() => { Alert.Content = AlertMessage; }));
                    Console.WriteLine(AlertMessage);
                    return true;
                }
                if (calendarCount > PurgeLimit)
                {
                    AlertMessage = "Purge all entries";
                    Dispatcher.BeginInvoke(new Action(() => { Alert.Content = AlertMessage; }));
                    Console.WriteLine(AlertMessage);
                    DeleteAll(false);
                    return true;
                }
                Console.WriteLine("Purge limit not yet reached: {0} | limit is {1}", calendarCount, PurgeLimit);

                //attempt random DeleteAll
                if (tokenSource.IsCancellationRequested) return false;
                var RandomIndex = Utilities.Data.RandomInt(0, calenderCompare.Count());
                var chosen = calenderCompare[RandomIndex];
                try
                {
                    AlertMessage = "Delete " + iteration + " on Subject: " + chosen.Subject;
                    Console.WriteLine(AlertMessage);
                    Dispatcher.BeginInvoke(new Action(() => { Alert.Content = AlertMessage; }));
                    var appointment = Appointment.Bind(Service, chosen.GetItemId());
                    appointment.Delete(DeleteMode.HardDelete);
                }
                catch
                {
                    AlertMessage = "Retry random DeleteAll";
                    Console.WriteLine(AlertMessage);
                    Dispatcher.BeginInvoke(new Action(() => { Alert.Content = AlertMessage; }));
                    isAdded = true;
                    return true;
                }
                return true;
            }
            isAdded = true;

            if (tokenSource.IsCancellationRequested) return false;
            return SendRandomAppointment(iteration);
        }

        private bool SendRandomAppointment(int iteration)
        {
            if (Exchange.TestService(Config.Email, Config.Password, Config.Webmail))
            {
                var RandomAppt =
                    Comparison.NewAppointmentFromComparison(GenerateComparionModel(iteration),
                        Service);
                try
                {
                    RandomAppt.Save();
                }
                catch
                {
                    Console.WriteLine("error saving");
                }
                
                return true;
            }
            return false;
        }

        private ExchComparison GenerateComparionModel(int iteration)
        {
            var randomMessage = new StringBuilder();
            for (int i = 0; i < 50; i++)
            {
                randomMessage.AppendLine(Utilities.StringFormatter.RandomString(25));
            }

            var randomCat = new List<string>();
            randomCat.Add("Green Category");
            randomCat.Add("Blue Category");
            randomCat.Add("Red Category");
            randomCat.Add("Yellow Category");
            randomCat.Add("Orange Category");
            randomCat.Add("Purple Category");
            var randomCatSelect = new List<string>();
            randomCatSelect.Add(randomCat[Utilities.Data.RandomInt(0, 6)]);
            randomCatSelect.Add(randomCat[Utilities.Data.RandomInt(0, 3)]);
            randomCatSelect.Add(randomCat[Utilities.Data.RandomInt(3, 6)]);
            var catagories = new StringList(randomCatSelect);
            var randomDate = Utilities.Date.RandomDate(Config.MinDays, Config.MaxDays);
            var RandomDateEnd = randomDate.AddHours(Utilities.Data.RandomInt(4, 48));
            var importance = Importance.High;
            var RandomImportance = new Random().Next(0, 3);
            switch (RandomImportance)
            {
                case 0:
                    importance = Importance.High; ;
                    break;
                case 1:
                    importance = Importance.Normal;
                    break;
                case 2:
                    importance = Importance.Low;
                    break;
            }

            AlertMessage = "Spam " + iteration + @" on: " + randomDate;
            Console.WriteLine(AlertMessage);
            Dispatcher.BeginInvoke(new Action(() => { Alert.Content = AlertMessage; }));
            var cm = new ExchComparison()
            {
                Subject = Config.Subject + ":" + iteration + ":" + Utilities.StringFormatter.RandomString(50),
                End = RandomDateEnd,
                Start = randomDate,
                Importance = importance,
                Location = Utilities.StringFormatter.RandomString(50)
            };
            cm.SetBody(Config.Body + ":" + iteration + ":" + randomMessage, true);
            cm.SetCatagories(catagories);
            return cm;
        }
        void save()
        {
            if (!_dataLoaded) return; //prevents data from saving during Config to controls right after a data load
            try
            {
                ControlsToData();
                Data.FileName = Config.DataName; //which file to save to
                Data.save(Config);
                Console.WriteLine("saved");
            }
            catch
            {
                Console.WriteLine("Invalid file format for saving");
            }   
            TextCursorToEnd(MaxDays_TB);
            TextCursorToEnd(MinDays_TB);
            TextCursorToEnd(rate_TB);
        }

        void load()
        {
            Config = Data.load() as ConfigModel;
            SpamControls(false);
            Spam_BT.IsEnabled = false;
            Task.Run(()=>
            {
                Service = Exchange.GetService(Config.Email, Config.Password, Config.Webmail);
                if (Service == null)
                {
                    MessageBox.Show("Cannot connect with your settings!");
                }
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    DataToControls();
                    SpamControls(true);
                    _dataLoaded = true;
                }));
            });
        }

        void DeleteAll(bool withMessage = true)
        {
            Action startDeleting = delegate()
            {           
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    SpamControls(false);
                    Spam_BT.IsEnabled = false;
                    AlertMessage = "Collecting calendar entries";
                    Console.WriteLine(AlertMessage);
                    Dispatcher.BeginInvoke(new Action(() => { Alert.Content = AlertMessage; }));
                }));
                if (!Exchange.TestService(Config.Email, Config.Password, Config.Webmail))
                {
                    return;
                }
                var calenderCompare = CalendarMirror.Comparison.CalendarToComparisonList(Service, 12, 12);
                var toBeDeleted = new List<ItemId>();
                var toBeDeletedSub = new List<string>();
                foreach (var c in calenderCompare)
                {
                    toBeDeleted.Add(c.GetItemId());
                    toBeDeletedSub.Add(c.Subject);
                }
                var jobs = new List<Task>();
                for (var i = 0; i < toBeDeleted.Count; i++)
                {
                    var subject = toBeDeletedSub[i];
                    var id = toBeDeleted[i];
                    var j = Task.Run(() =>
                    {
                        try
                        {
                            AlertMessage = "Delete: " + subject;
                            Console.WriteLine(AlertMessage);
                            Dispatcher.BeginInvoke(new Action(() => { Alert.Content = AlertMessage; }));
                            var appointment = Appointment.Bind(Service, id);
                            appointment.Delete(DeleteMode.HardDelete);
                        }
                        catch
                        {
                            AlertMessage = "Delete failed: " + subject;
                            Console.WriteLine(AlertMessage);
                            Dispatcher.BeginInvoke(new Action(() => { Alert.Content = AlertMessage; }));
                        }
                    }); 
                    jobs.Add(j); 
                }
                Task.WaitAll(jobs.ToArray());
                Dispatcher.BeginInvoke(new Action(() => {
                    SpamControls(true);
                    Spam_BT.IsEnabled = true;
                })); 
            };
            if (withMessage)
            {
                Utilities.Dialog.YesNo(
                    "Are you sure you want to DeleteAll all calendar entries within a two year scope? ",
                    startDeleting);
            }
            else
            {
                startDeleting.Invoke();
            }
            
        }
        #endregion


        #region ControlInterface
        private void Quick_BT_Click(object sender, RoutedEventArgs e)
        {
            Quickie();
        }
        private async void DelCal_BT_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(()=>{ DeleteAll(); });
        }
        private void MinDays_TB_TextChanged(object sender, TextChangedEventArgs e)
        {
            SettingsValidator(sender);
        }

        private void MaxDays_TB_TextChanged(object sender, TextChangedEventArgs e)
        {
            SettingsValidator(sender);
        }

        private void rate_TB_TextChanged(object sender, TextChangedEventArgs e)
        {
            SettingsValidator(sender, 9);
        }
        private void cc_TB_TextChanged(object sender, TextChangedEventArgs e)
        {
            SettingsValidator(sender, 4);
        }
        private void duration_TB_TextChanged(object sender, TextChangedEventArgs e)
        {
            SettingsValidator(sender, 9);
        }
        private void MinDays_TB_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender);
        }

        private void MaxDays_TB_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender);
        }

        private void sub_TB_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender);
        }

        private void body_TB_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender);
        }

        private void location_TB_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender);
        }

        private void rate_TB_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender);
        }

        private void email_TB_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender);
        }

        private void Password_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender, true);
        }
        private void cc_TB_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender);
        }

        private void duration_TB_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender);
        }
        private void WB_TB_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender);
        }
        private void email_TB_LostFocus(object sender, RoutedEventArgs e)
        {
            AutoFillWebmail(email_TB, WB_TB);
        }
        private void Delete_CB_Click(object sender, RoutedEventArgs e)
        {
            save();
        }
        private void Reg_CB_Click(object sender, RoutedEventArgs e)
        {
            save();
            RegulatedControls(Config.Regulated);
        }

        #endregion




        #region Common

        //selects all on any textbox
        void SelectAll(object sender, bool isPW = false)
        {
            //for event, use On GotMouseCapture and GotFocus
            if (!isPW)
            {
                var s = sender as TextBox;
                if (s != null)
                {
                    s.Focus();
                    s.SelectAll();
                }
            }
            else
            {
                var s = sender as PasswordBox;
                if (s != null)
                {
                    s.Focus();
                    s.SelectAll();
                }
            }
            save();
        }

        void SettingsValidator(object TextboxField, int limit = 3)
        {
            if (!_dataLoaded) return;
            var field = TextboxField as TextBox;
            var valid = Utilities.StringValidator.OnlyNumber(field.Text, limit) || field.Text == "";
            if (!valid)
            {
                Console.WriteLine("Invalid field");
                load();
                return;
            }
            if (field.Text == "")
            {
                Console.WriteLine("No field");
            }
            else
            {
                save();
                return;
            }
        }
        void TextCursorToEnd(TextBox t)
        {
            t.CaretIndex = t.Text.Length;
            t.ScrollToEnd();
        }
        void AutoFillWebmail(TextBox emailTB, TextBox webmail)
        {
            if (webmail != null)
            {
                if (Utilities.StringValidator.IsEmail(emailTB.Text))
                {
                    webmail.Text = "webmail." + Utilities.StringFormatter.GetDomainFromEmail(emailTB.Text, true);
                    emailTB.Background = Brushes.White;
                }
                else
                {
                    emailTB.Background = Brushes.PaleVioletRed;
                }

            }
        }



        #endregion

        
    }

}
