﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CalendarMirror.Annotations;

namespace CalendarSpammer
{
    [Serializable]
    public class ConfigModel: INotifyPropertyChanged
    {
        private string _DataName;
        public string DataName
        {
            get { return _DataName; }
            set
            {
                if (value != _DataName)
                {
                    _DataName = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _Email;

        public string Email
        {
            get { return _Email; }
            set
            {
                if (value != _Email)
                {
                    _Email = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _Webmail;

        public string Webmail
        {
            get { return _Webmail; }
            set
            {
                if (value != _Webmail)
                {
                    _Webmail = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _Password;

        public string Password
        {
            get { return _Password; }
            set
            {
                if (value != _Password)
                {
                    _Password = value;
                    OnPropertyChanged();
                }
            }
        }

        private int _MinDays;

        public int MinDays
        {
            get { return _MinDays; }
            set
            {
                if (value != _MinDays)
                {
                    _MinDays = value;
                    OnPropertyChanged();
                }
            }
        }

        private int _MaxDays;

        public int MaxDays
        {
            get { return _MaxDays; }
            set
            {
                if (value != _MaxDays)
                {
                    _MaxDays = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _Subject;

        public string Subject
        {
            get { return _Subject; }
            set
            {
                if (value != _Subject)
                {
                    _Subject = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _Body;

        public string Body
        {
            get { return _Body; }
            set
            {
                if (value != _Body)
                {
                    _Body = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _Location;

        public string Location
        {
            get { return _Location; }
            set
            {
                if (value != _Location)
                {
                    _Location = value;
                    OnPropertyChanged();
                }
            }
        }

        private int _Rate;

        public int Rate
        {
            get { return _Rate; }
            set
            {
                if (value != _Rate)
                {
                    _Rate = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool _Delete;

        public bool Delete
        {
            get { return _Delete; }
            set
            {
                if (value != _Delete)
                {
                    _Delete = value;
                    OnPropertyChanged();
                }
            }
        }

        private int _Concurrent;

        public int Concurrent
        {
            get { return _Concurrent; }
            set
            {
                if (value != _Concurrent)
                {
                    _Concurrent = value;
                    OnPropertyChanged();
                }
            }
        }

        private int _Duration;

        public int Duration
        {
            get { return _Duration; }
            set
            {
                if (value != _Duration)
                {
                    _Duration = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool _Regulated;

        public bool Regulated
        {
            get { return _Regulated; }
            set
            {
                if (value != _Regulated)
                {
                    _Regulated = value;
                    OnPropertyChanged();
                }
            }
        }

        public static ConfigModel DefaultConfig()
        {
            var r = new ConfigModel();
            r.Email = "[EMAIL]";
            r.Webmail = "[WEBMAIL]";
            r.Password = "";
            r.MinDays = 5;
            r.MaxDays = 5;
            r.Subject = "Spam Subject";
            r.Body = "Spam Body";
            r.Location = "Spam Location";
            r.Rate = 500;
            r.DataName = "data.ecspam";
            r.Duration = 1;
            r.Concurrent = 20;
            r.Delete = true;
            r.Regulated = true;
            return r;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
