﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void RollLog_Amount_Accumulate()
        {
            //oldest log.3 > log.2 > log.1 > log newest
            var logger = new FileLogL4N(FileLogL4N.GenerateFileName(folder:"logtest", isCSV:true), 3, "500KB");
            var testAmount = 1000;

            for (int i = 0; i < testAmount; i++)
            {
                logger.L.Debug(StringFormatter.RandomString(5000));
            }
        }

        [TestMethod]
        public void RollLog_ReverseParent_Output()
        {
            var logger = new FileLogL4N(FileLogL4N.GenerateFileName(folder: "logtest", upParent: 1,isCSV: true), 3, "1KB");
            var testAmount = 1000;

            for (int i = 0; i < testAmount; i++)
            {
                logger.L.Debug("MESSAGE " + i);
            }
        }

        [TestMethod]
        public void NamingLog_ReverseParent_ReversePath()
        {
            var logName = FileLogL4N.GenerateFileName(folder:"logtest", upParent:1);
            Trace.WriteLine(logName);
            Assert.AreEqual("C:\\Users\\viluan\\Source\\Repos\\CalendarMirror\\CalendarMirror\\UnitTest\\bin\\logtest\\log.txt", logName
                );
        }

        [TestMethod]
        public void ReverseParent_ReversePath()
        {
            var original =
                "C:\\Users\\viluan\\Source\\Repos\\CalendarMirror\\CalendarMirror\\UnitTest\\bin\\Debug\\logtest\\";
            var newPath = StringFormatter.FolderUpParent(original, 1);
            Trace.WriteLine(newPath);
            Assert.AreEqual(newPath, "C:\\Users\\viluan\\Source\\Repos\\CalendarMirror\\CalendarMirror\\UnitTest\\bin\\Debug\\");
        }

        [TestMethod]
        public void GetPIDofServiceAndSelf()
        {
            Trace.WriteLine("Self: " + Process.GetCurrentProcess().Id);
            var list = Process.GetProcessesByName("CalendarMirror");
            Trace.WriteLine("List length: " + list.Length);
            var exclude = new List<Process>();
            exclude.Add(Process.GetCurrentProcess());
            var exList = list.Except(exclude);
            foreach (var process in list)
            {
                Trace.WriteLine(process.Id + " : " + process.ProcessName);
                Processes.KillProcessByID(process.Id);  
            }
        }
        [TestMethod]
        public void LinqTest()
        {
            var o = new List<string>() {"a", "b", "c"};
            var i = new List<string>() {"a", "b"};
            var o1 = o.ToArray();
            var i1 = i.ToArray();
            var ex = o1.Except(i1);
            foreach (var s in ex)
            {
                Trace.WriteLine(s);
            }
        }
    }
}
