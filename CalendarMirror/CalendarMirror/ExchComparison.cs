﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Exchange.WebServices.Data;
using Utilities;
using Task = System.Threading.Tasks.Task;

namespace CalendarMirror
{
    #region Model

    public interface IComparison
    {
        string BodyHTML { get; set; }
        int BodyHash { get; set; }
        int dbTag { get; set; }
        string ItemId { get; set; }
        string Subject { get; set; }
        DateTime End { get; set; }
        DateTime Start { get; set; }
        byte[] Catagories { get; set; }
        Importance Importance { get; set; }
        string Location { get; set; }
        bool IsNew { get; set; }
        bool IsSafe { get; set; }
        string Body { get; set; }

        StringList GetCatagories();
        void SetCatagories(StringList List);
        void SetItemId(ItemId id);
        ItemId GetItemId();
        void SetBody(MessageBody MBody, bool skipConversion = false);
        int CatagoriesSize { get; set; }
        MessageBody GetBody();
        Appointment OriginalAppointment { get; set; }
        // public abstract byte[] StringListToByteArray(StringList stringlist);
    }
    public class ExchComparison: IComparison
    {
        public string BodyHTML { get; set; }
        public int BodyHash { get; set; }
        public int dbTag { get; set; }
        public string ItemId { get; set; }
        public string Subject { get; set; }
        public DateTime End { get; set; }
        public DateTime Start { get; set; }
        public byte[] Catagories { get; set; }
        public Importance Importance { get; set; }
        public string Location { get; set; }
        public bool IsNew { get; set; }
        public bool IsSafe { get; set; }
        public string Body { get; set; }

        public StringList GetCatagories()
        {
            var ls = Utilities.Data.ByteArrayToObject(Catagories) as string[];
            var ls1 = new StringList();
            for (var i = 0; i < ls.Length; i++)
            {
                ls1.Add(ls[i]);
            }
            return ls1;
        }
        public void SetCatagories(StringList List)
        {
            Catagories = StringListToByteArray(List);
            int size = 0;
            foreach (var c in Catagories)
            {
                size = c + size;
            }
            CatagoriesSize = size;
        }

        public void SetItemId(ItemId id)
        {
            ItemId = id.ToString();
        }
        public ItemId GetItemId()
        {
            return new ItemId(ItemId);
        }

        public void SetBody(MessageBody MBody, bool skipConversion = false)
        {
            if (!skipConversion)
            {
                BodyHTML = Utilities.StringFormatter.HTMLBodyExtract(MBody);
            }
            else
            {
                BodyHTML = "";
            }
            Body = MBody.Text;
        }
        public MessageBody GetBody()
        {
            return new MessageBody(Body);
        }

        public int CatagoriesSize { get; set; }
        public Appointment OriginalAppointment { get; set; }

        private byte[] StringListToByteArray(StringList stringlist)
        {
            var ls = new string[stringlist.Count];
            for (var i = 0; i < stringlist.Count; i++)
            {
                ls[i] = stringlist[i];
            }
            return Utilities.Data.ObjectToByteArray(ls);
        }
    }


    #endregion



    public class Comparator
    {
        private ExchangeService Target;
        private ExchangeService Source;
        private List<ExchComparison> SCM;
        private List<ExchComparison> TCM;
        private List<Appointment> TargetDiscard;
        private List<Appointment> TargetAdd;
        public Comparator(ExchangeService source, ExchangeService target)
        {
            Target = target;
            Source = source;
            SCM = new List<ExchComparison>();
            TCM = new List<ExchComparison>();
            TargetDiscard = new List<Appointment>();
            TargetAdd = new List<Appointment>();
        }

        public void Reset()
        {
            SCM.Clear();
            TCM.Clear();
            TargetDiscard.Clear();
            TargetAdd.Clear();
        }
        public void AddCalendarSource(FindItemsResults<Appointment> sourceCalendar)
        {
            if (sourceCalendar == null) return;
            var e = sourceCalendar.GetEnumerator();
            e.Reset();
            while (e.MoveNext())
            {
                SCM.Add(Comparison.AppointmentToComparisonModel(e.Current));
            }
            e.Dispose();
        }

        public void AddCalendarTarget(FindItemsResults<Appointment> targetCalendar)
        {
            if (targetCalendar == null) return;
            var e = targetCalendar.GetEnumerator();
            e.Reset();
            while (e.MoveNext())
            {
                TCM.Add(Comparison.AppointmentToComparisonModel(e.Current));
            }
            e.Dispose();
        }

        public string Deduplicate()
        {
            var error = "";
            var Grouping = TCM.GroupBy(x => new { CatagoriesByteSize = x.CatagoriesSize, x.Start, x.End, x.Subject, x.Location, x.Importance, x.BodyHTML }); //x.Catagories, x.Start, x.End, x.Subject, x.Location, x.Importance, x.BodyHTML 
            try
            {
                foreach (var group in Grouping)
                {
                    var marked = false;
                    // Console.WriteLine("Group size " + group.Count());
                    foreach (var i in group
                    ) //each of these group unique, but their items is all the same base on gets parameter
                    {
                        if (!marked)
                        {
                            marked = true;
                            i.IsSafe = true;
                            continue;
                        }
                        TargetDiscard.Add(i.OriginalAppointment);
                        Console.WriteLine("Added in duplication: " + i.Subject);
                    }
                }
                TCM.RemoveAll(x => !x.IsSafe);
            }
            catch
            {
                error = "Error in IsSafe marking";
            }

            return error;  
        }

        public string Compare()
        {
            var error = "";

            //safe targets
            var targetMatchSource = TCM.Where(t => SCM.Any(s =>
                s.CatagoriesSize == t.CatagoriesSize &&
                t.Start == s.Start &&
                t.End == s.End &&
                s.Importance == t.Importance &&
                s.Location == t.Location &&
                s.BodyHTML == t.BodyHTML
            )).ToList();

            //not safe target
            var targetNotMatchSource = TCM.Except(targetMatchSource);

            var sourceMatchTarget = SCM.Where(s => TCM.Any(t =>
                t.CatagoriesSize == s.CatagoriesSize &&
                t.Subject == s.Subject &&
                t.Start == s.Start &&
                t.End == s.End &&
                t.Importance == s.Importance &&
                t.Location == s.Location &&
                t.BodyHTML == s.BodyHTML
            ));

            //New for target
            var sourceNotMatchTarget = SCM.Except(sourceMatchTarget);

            //mark same as safe  
            try
            {
                foreach (var t in targetMatchSource)
                {
                    t.IsSafe = true;
                }
            }
            catch
            {
                error = "Exception at marking TCM safe TargetMatchSource.";
            }

            //mark not safe 
            try
            {
                foreach (var t in targetNotMatchSource)
                {
                    t.IsSafe = false;
                }
            }
            catch
            {
                error = error + " Exception at marking not safe for TargetNotMatchSource";
            }

            //add new to target     
            try
            {
                foreach (var s in sourceNotMatchTarget)
                {
                    TargetAdd.Add(Comparison.ComparisonModelToAppointment(s, Target));
                }
            }
            catch
            {
                error = error + " Exception at creating new targets.";
            }

            //add discard for not safe  
            try
            {
                foreach (var t in TCM)
                {
                    if (!t.IsSafe)
                    {
                        TargetDiscard.Add(t.OriginalAppointment);
                    }
                }
                TCM.RemoveAll(x => !x.IsSafe);
            }
            catch
            {
                error = error + " Exception at add discard.";
            }
            Console.WriteLine("Will discard: " + TargetDiscard.Count);
            Console.WriteLine("Will add: " + TargetAdd.Count);
            return error;
        }

        public Tuple<List<Result>, string> Discard()
        { 
            var error = "";
            var results = new List<Result>(); 
            if (!TargetDiscard.Any()) return Tuple.Create(results, error);

            var T = new List<Task<Result>>();
            var cDel = new Calendar();
 
            try
            {
                foreach (var i in TargetDiscard)
                {
                    T.Add(Task.Run(() => cDel.AsyncDeleteCalenderItem(Target, i.Subject, i.Id.ToString())));
                }
                Task.WaitAll(T.ToArray());
            }
            catch
            {
                error = "Error in target calendar delete";
            }
            
            foreach (var task in T)
            {
                results.Add(task.Result);
            }
            return Tuple.Create(results, error);
        }

        public List<Result> Add()
        {
            var results = new List<Result>();
            var T = new List<Task<Result>>();

            var cDel = new Calendar();
            if (TargetAdd.Any())
            {
                foreach (var add in TargetAdd)
                {
                    T.Add(Task.Run(() => cDel.AsyncAddCalenderItem(add)));
                }
                Task.WaitAll(T.ToArray());
                foreach (var task in T)
                {
                    results.Add(task.Result);
                }
            }
            return results;
        }
    }

   

    

    public class Comparison
    {
        public static ExchComparison AppointmentToComparisonModel(Appointment appt)
        {
            var c = new ExchComparison();
            c.Subject = appt.Subject;
            c.End = appt.End;
            c.Start = appt.Start;
            c.SetCatagories(appt.Categories);
            c.Importance = appt.Importance;
            c.Location = appt.Location;
            c.SetBody(Calendar.GetBodyByID(appt.Service, appt.Id));
            c.SetItemId(appt.Id);
            c.IsSafe = false;
            c.OriginalAppointment = appt;
            return c;
        }

        //conversions for spammer
        public static Appointment ComparisonModelToAppointment(ExchComparison s, ExchangeService e)
        {
            var appt = new Appointment(e);
            appt.Subject = s.Subject;
            appt.End = s.End;
            appt.Start = s.Start;
            appt.Categories = s.GetCatagories();
            appt.Importance = s.Importance;
            appt.Location = s.Location;
            appt.Body = s.GetBody();
            return appt;
        }

        public static ExchComparison NewComparisonFromAppointment(Appointment appt, string body)
        {
            var r = new ExchComparison();
            r.Subject = appt.Subject;
            r.End = appt.End;
            r.Start = appt.Start;
            r.SetCatagories(appt.Categories);
            r.Importance = appt.Importance;
            r.Location = appt.Location;
            r.SetBody(body);
            r.SetItemId(appt.Id);
            return r;
        }

        public static Appointment NewAppointmentFromComparison(ExchComparison cm, ExchangeService Profile)
        {
            var r = new Appointment(Profile);
            r.Subject = cm.Subject;
            r.End = cm.End;
            r.Start = cm.Start;
            r.Categories = cm.GetCatagories();
            r.Importance = cm.Importance;
            r.Location = cm.Location;
            r.Body = cm.GetBody();
            return r;
        }

        #region Depreciated
        #region AppointmentToIComparison
        ////most is the same
        //public static SourceEntity AppointmentToIComparisonS(Appointment appt, string body, int dbTag)
        //{
        //    var c = new SourceEntity();
        //    c.Subject = appt.Subject;
        //    c.End = appt.End;
        //    c.Start = appt.Start;
        //    c.SetCatagories(appt.Categories);
        //    c.Importance = appt.Importance;
        //    c.Location = appt.Location;
        //    c.SetBody(body);
        //    c.SetItemId(appt.Id);
        //    c.dbTag = dbTag;
        //    return c;
        //}
        //public static TargetEntity AppointmentToIComparisonT(Appointment appt, string body, int dbTag)
        //{
        //    var c = new TargetEntity();
        //    c.Subject = appt.Subject;
        //    c.End = appt.End;
        //    c.Start = appt.Start;
        //    c.SetCatagories(appt.Categories);
        //    c.Importance = appt.Importance;
        //    c.Location = appt.Location;
        //    c.SetBody(body);
        //    c.SetItemId(appt.Id);
        //    c.dbTag = dbTag;
        //    return c;
        //}
        #endregion

        #region Source=>Target=>Appt

        //public static TargetEntity SourceEntityToTargetEntity(SourceEntity s)
        //{
        //    var t = new TargetEntity();
        //    t.dbTag = s.dbTag;
        //    t.Subject = s.Subject;
        //    t.End = s.End;
        //    t.Start = s.Start;
        //    t.Catagories = s.Catagories;
        //    t.Importance = s.Importance;
        //    t.Location = s.Location;
        //    t.SetBody(s.GetBody());
        //    t.SetItemId("NULL_" + Utilities.StringFormatter.RandomString(10)); //define when appt is made
        //    t.IsSafe = true;
        //    t.IsNew = true;
        //    return t;
        //}

        //public static Appointment TargetEntityToAppointment(TargetEntity t, ExchangeService e)
        //{
        //    var appt = new Appointment(e);
        //    appt.Subject = t.Subject;
        //    appt.End = t.End;
        //    appt.Start = t.Start;
        //    appt.Categories = t.GetCatagories();
        //    appt.Importance = t.Importance;
        //    appt.Location = t.Location;
        //    appt.Body = t.GetBody();
        //    return appt;
        //}

        #endregion


        //Gets Exchange calender in comparison form
        public static List<ExchComparison> CalendarToComparisonList(ExchangeService Profile, int BackMonths, int FowardMonths)
        {
            var calender = Calendar.GetCalendar(Profile, BackMonths, FowardMonths);
            if (calender == null) return new List<ExchComparison>();

            var l = new List<ExchComparison>();
            var T = new List<Task>();
            foreach (var i in calender)
            {
                T.Add(Task.Run(() =>
                {
                    try
                    {
                        var thisMessageBody = Calendar.GetBodyByID(Profile, i.Id);
                        l.Add(NewComparisonFromAppointment(i, thisMessageBody));
                    }
                    catch
                    {
                        Console.WriteLine("Could not add appointment");
                    }
                }));
            }
            Task.WaitAll(T.ToArray());
            return l;
        }

        ////depreciated comparison
        //public static bool CompareComparisons(ComparisonModelX cm, ComparisonModelX cm2)
        //{
        //    //Console.WriteLine("Comparing Calendar to ComparisonModelX");
        //    try
        //    {
        //        if (Utilities.Data.isEqualExtra(cm2.Subject, cm.Subject))
        //        {
        //            if (Utilities.Data.isEqualExtra(cm2.End, cm.End))
        //            {
        //                if (Utilities.Data.isEqualExtra(cm2.Start, cm.Start))
        //                {
        //                    if (Utilities.Data.isEqualExtra(cm2.Catagories, cm.Catagories))
        //                    {
        //                        if (Utilities.Data.isEqualExtra(cm2.Importance, cm.Importance))
        //                        {
        //                            if (Utilities.Data.isEqualExtra(cm2.Location, cm.Location))
        //                            {
        //                                //Console.WriteLine("Match: " + cm.Subject);
        //                                return true;
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        Console.WriteLine("Error in comparison");
        //        return false;
        //    }
        //    return false;
        //}

        #endregion
    }
}
