﻿using System;
using System.Linq;
using System.Threading;
using Microsoft.Exchange.WebServices.Data;

namespace CalendarMirror
{
    public class Exchange
    {
        public static ExchangeService FTestServiceSource()
        {
            var t = TestService("viluan@dvicomm.com", "Network1", "webmail.dvicomm.com");
            return GetService("viluan@dvicomm.com", "Network1", "webmail.dvicomm.com");
        }

        public static ExchangeService FTestServiceTarget()
        {
            var t = TestService("viluan@fensterstock.com", "fp4100,.", "webmail.fensterstock.com");
            return GetService("viluan@fensterstock.com", "fp4100,.", "webmail.fensterstock.com");
        }

        /// <summary>
        /// Get Exchange service
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="webmail">example: webmail.fensterstock.com</param>
        /// <returns></returns>
        public static ExchangeService GetService(string email, string password, string webmail, int timeout = 5, CancellationTokenSource tokenSource = null)
        {
            try
            {
                ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
                service.Credentials = new WebCredentials(email, password);
                service.AutodiscoverUrl(email, RedirectionUrlValidationCallback);
                service.UseDefaultCredentials = false;
                var actualURL = @"https://" + webmail + @"/EWS/Exchange.asmx";
                service.Url = new System.Uri(actualURL);
                return service;
            }
            catch
            {
                var msg = "Cannot connect to " + email;
                while (!TestService(email, password, webmail))
                {
                    if(MirrorAll.Mirrors.Any())MirrorAll.Mirrors[0].log(msg, true);
                    Console.WriteLine(msg);
                    Thread.Sleep(2000);
                    timeout--;
                    if (timeout < 1)
                    {
                        return null;
                    }
                    if (tokenSource != null)
                    {
                        if (tokenSource.IsCancellationRequested) return null;
                    }
                }
                return GetService(email, password, webmail);
            } 
        }

        public static bool TestService(string email, string password, string webmail)
        {
            try
            {
                ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
                service.Credentials = new WebCredentials(email, password);
                service.AutodiscoverUrl(email, RedirectionUrlValidationCallback);
                service.UseDefaultCredentials = false;
                var actualURL = @"https://" + webmail + @"/EWS/Exchange.asmx";
                service.Url = new System.Uri(actualURL);  
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;

            Uri redirectionUri = new Uri(redirectionUrl);

            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }

    }
}

//public static void FTechsupportSend(string recipient, string subject, string body)
//{
//    EmailMessage email = new EmailMessage(FService());
//    email.ToRecipients.Add(recipient);
//    email.Subject = subject;
//    email.Body = new MessageBody(body);
//    email.Send();
//}
