﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Runtime.CompilerServices;
using CalendarMirror.Annotations;

namespace CalendarMirror
{
    /// <summary>
    /// Holds save settings
    /// </summary>
    [Serializable]
    public class ConfigModel:INotifyPropertyChanged
    {
        private int _BackMonths;

        public int BackMonths
        {
            get { return _BackMonths; }
            set
            {
                if (value != _BackMonths)
                {
                    _BackMonths = value;
                    OnPropertyChanged();
                }
            }
        }

        private int _ForwardMonths;

        public int ForwardMonths
        {
            get { return _ForwardMonths; }
            set
            {
                if (value != _ForwardMonths)
                {
                    _ForwardMonths = value;
                    OnPropertyChanged();
                }
            }
        }

        private int _Frequency;

        public int Frequency
        {
            get { return _Frequency; }
            set
            {
                if (value != _Frequency)
                {
                    _Frequency = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool _RealTime;

        public bool RealTime
        {
            get { return _RealTime; }
            set
            {
                if (value != _RealTime)
                {
                    _RealTime = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _AdminUser;

        public string AdminUser
        {
            get { return _AdminUser; }
            set
            {
                if (value != _AdminUser)
                {
                    _AdminUser = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _Password;

        public string Password
        {
            get { return _Password; }
            set
            {
                if (value != _Password)
                {
                    _Password = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _Domain;

        public string Domain
        {
            get { return _Domain; }
            set
            {
                if (value != _Domain)
                {
                    _Domain = value;
                    OnPropertyChanged();
                }
            }
        }

        private int _LogKeep;

        public int LogKeep
        {
            get { return _LogKeep; }
            set
            {
                if (value != _LogKeep)
                {
                    _LogKeep = value;
                    OnPropertyChanged();
                }
            }
        }

        public List<ProfileModel> Profiles { get; set; }

        public static ConfigModel GetNew()
        {
            return new ConfigModel()
            {
                Profiles = ProfileModel.GetNew(),
                BackMonths = 6,
                ForwardMonths = 6,
                Frequency = 1,
                AdminUser = "",
                Password = "",
                Domain = "",
                RealTime = false,
                LogKeep = 0
            };
        }

        public BindingList<string> Names()
        {
            var l = new BindingList<string>();
            foreach (var n in Profiles)
            {
                l.Add(n.Title);
            }
            return l;
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    /// <summary>
    /// Holds each mirror configurations
    /// </summary>
    [Serializable]
    public class ProfileModel
    {
        public bool isEnabled { get; set; }

        //source calendar
        public string Title { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string webmail { get; set; } //url
        public IPAddress webmailIP { get; set; } //resolvable IP

        //target calendar
        public string email1 { get; set; }
        public string password1 { get; set; }
        public string webmail1 { get; set; }
        public IPAddress webmailIP1 { get; set; }

        public static List<ProfileModel> GetNew()
        {
            return new List<ProfileModel>();
        }

        public static void RemoveByTitle(List<ProfileModel> List, string name)
        {
            var temp = new List<ProfileModel>();
            foreach (var i in List)
            {
                if (i.Title == name)
                {
                    temp.Add(i);
                }
            }
            foreach (var i in temp)
            {
                List.Remove(i);
            }
        }

        public static ProfileModel GetByTitle(List<ProfileModel> List, string name)
        {
            foreach (var i in List)
            {
                if (i.Title == name)
                {
                    return i;
                }
            }
            return null;
        }
    }
}
