﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Exchange.WebServices.Data;
using Task = System.Threading.Tasks.Task;

namespace CalendarMirror
{
    public class Calendar
    {
        public async Task<Utilities.Result> AsyncAddCalenderItem(Appointment appt)
        {
            var result = await Task.Run(() =>
            {
                try
                {
                    appt.Save(SendInvitationsMode.SendToNone);
                    return new Utilities.Result(appt.Subject).Method("Add").Success();
                }
                catch
                {
                    return new Utilities.Result(appt.Subject).Method("Add").Failed();
                }
            });
            return result;
        }
        public async Task<Utilities.Result> AsyncDeleteCalenderItem(ExchangeService Profile, string subject, string id)
        {
            var result = await Task.Run(() =>
            {
                try
                {
                    var appointment = Appointment.Bind(Profile, id);
                    appointment.Delete(DeleteMode.HardDelete);
                    return new Utilities.Result(subject).Method("Delete").Success();
                }
                catch
                {
                    return new Utilities.Result(subject).Method("Delete").Failed();
                }
            });
            return result;
        }

        public static List<string> GetSubjectListInCalender(ExchangeService Profile)
        {
            var c = GetCalendar(Profile, MirrorAll.configs.BackMonths, MirrorAll.configs.ForwardMonths);
            if(c == null) return new List<string>();

            var s = new List<string>();
            foreach (var i in c)
            {
                s.Add(i.Subject);
            }
            return s;
        }
        public static List<DateTime> GetStartListInCalender(ExchangeService Profile)
        {
            var c = GetCalendar(Profile, MirrorAll.configs.BackMonths, MirrorAll.configs.ForwardMonths);
            if (c == null) return new List<DateTime>();

            var s = new List<DateTime>();
            foreach (var i in c)
            {
                s.Add(i.Start);
            }
            return s;
        }
        public static bool SubjectExistInCalendar(ExchangeService Profile, string subject)
        {
            var c = GetCalendar(Profile, MirrorAll.configs.BackMonths, MirrorAll.configs.ForwardMonths);
            if (c == null) return false;

            foreach (var a in c)
            {
                if (a.Subject == subject)
                {
                    return true;
                }
            }
            return false;
        }

        public void DeleteCalendarItems(ExchangeService Profile, List<ItemId> DeleteAppointments)
        {
            var T = new List<Task>();
            foreach (var id in DeleteAppointments)
            {
                var subT = Task.Run(() =>
                {
                    try
                    {
                        Console.WriteLine("Deleting ID: " + id);
                        var appointment = Appointment.Bind(Profile, id);
                        appointment.Delete(DeleteMode.HardDelete);
                    }
                    catch
                    {
                        Console.WriteLine("Deleting has failed.");
                    }
                });
                T.Add(subT);
            }
            Task.WaitAll(T.ToArray());
        }
        public static FindItemsResults<Appointment> GetCalendar(ExchangeService profile, int backMonths, int forwardMonths, int limits = 0)
        {
            Console.WriteLine("Getting Calendar");
            DateTime startDate = DateTime.Now.AddMonths(-backMonths);
            DateTime endDate = DateTime.Now.AddMonths(forwardMonths);
            
            FindItemsResults<Appointment> appointments = null;
            CalendarFolder calendar = null;
            try
            {
                calendar = CalendarFolder.Bind(profile, WellKnownFolderName.Calendar, new PropertySet());
            }
            catch
            {
                //possible error: The request failed. The remote server returned an error: (401) Unauthorized. for DVI
                Console.WriteLine("Unable to get calendarfolder");
                return appointments;
            }
            CalendarView cView;

            if (limits < 1)
            {
                cView = new CalendarView(startDate, endDate);
            }
            else
            {
                cView = new CalendarView(startDate, endDate, limits);
            }

            cView.PropertySet = new PropertySet(
                AppointmentSchema.Subject,
                AppointmentSchema.End,
                AppointmentSchema.Start,
                AppointmentSchema.Id,
                AppointmentSchema.Categories,
                AppointmentSchema.Importance,
                AppointmentSchema.Location
                );
            //appointments = calendar.FindAppointments(cView);
            try
            {
                appointments = calendar.FindAppointments(cView);
            }
            catch
            {
                Console.WriteLine("Time out from server");
            }
            //Console.WriteLine("Appointment Retreived: " + appointments.Count());

            return appointments; //make sure all this reference gets a null handled
        }

        public static MessageBody GetBodyByID(ExchangeService Profile, ItemId ID)
        {
            try
            {
                var appointment = Appointment.Bind(Profile, ID);
                return appointment.Body;
            }
            catch
            {
                return "";
            }
        }

        public static void DeleteTestCalendar(int BackMonths, int FowardMonths)
        {
            var T = new List<Task>();
            var c = GetCalendar(Exchange.FTestServiceSource(), BackMonths, FowardMonths, 500);
            if (c == null) return;

            foreach (var i in c)
            {
                var subT = Task.Run(() =>
                {
                    var appointment = Appointment.Bind(Exchange.FTestServiceSource(), i.Id);
                    Console.WriteLine("Test Delete: " + i.Subject);
                    appointment.Delete(DeleteMode.HardDelete);
                });
                T.Add(subT);
            }
            Task.WaitAll(T.ToArray());
            Console.WriteLine("Test calendar has cleared");
        }
    }
}
