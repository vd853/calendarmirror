﻿using System;
using System.Windows;
using Topshelf;

namespace CalendarMirror
{
    public partial class Starter
    {
        public void Start(object sender, StartupEventArgs e)
        {
            if (e.Args.Length < 1)
            {
                Console.WriteLine("use WPF");
                var w = new MainWindow();
                w.ShowDialog();
                return;
            }

            HostFactory.Run(x => //1
            {
                x.Service<ServiceMode>(s => //2
                {
                    s.ConstructUsing(m => new ServiceMode()); //3
                    s.WhenStarted(m => m.Start()); //4    
                    s.WhenStopped(m => m.Stop());  
                    x.RunAsLocalSystem(); //6    
                    x.SetDescription("Mirrors Exchange Calendars"); //7
                    x.SetDisplayName("Calendar Mirror"); //8
                    x.SetServiceName("Calendar Mirror"); //9
                });
            });
        }
    }
    class ServiceMode
    {
        public static bool isUninstalledTriggered;
        private MirrorAll Job;
        public ServiceMode()
        {

        }

        public void Start()
        {
            Job = new MirrorAll();
            Job.Start();
        }

        public void Stop()
        {
            Job.Stop();
            Environment.Exit(0);
        }
    }
}
