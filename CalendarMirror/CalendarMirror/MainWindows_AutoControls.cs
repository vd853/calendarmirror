﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CalendarMirror
{ 
    public partial class MainWindow : Window
    {
        #region ControlAutomations

        void InitiControls()
        {
            //only checkbox
            Realtime_CB.Checked += OnRealTimeChecker;
            Realtime_CB.Unchecked += OnRealTimeChecker;

            TextBoxes = new List<TextBox>();
            PasswordBoxes = new List<PasswordBox>();
            TextboxesNum = new List<TextBox>();
            TextboxesNum.Add(BackMonth_TB);
            TextboxesNum.Add(FowardMonth_TB);
            TextboxesNum.Add(Freq_TB);
            TextboxesNum.Add(Log_TB);

            TextBoxes.Add(email_TB);
            PasswordBoxes.Add(pw_TB);
            TextBoxes.Add(wb_TB);
            TextBoxes.Add(email_TB1);
            PasswordBoxes.Add(pw_TB1);
            TextBoxes.Add(wb_TB1);
            //TextBoxes.Add(User_TB);
            //TextBoxes.Add(Domain_TB);
            //PasswordBoxes.Add(Password_TB);

            //Password_TB.KeyUp += OnPasswordKey;

            foreach (var textBox in TextBoxes)
            {
                textBox.GotFocus += OnFieldSelectAllFocus;
                textBox.GotMouseCapture += OnFieldSelectAllFocus;
                textBox.TextChanged += OnTextboxChange;

            }
            foreach (var passwordBox in PasswordBoxes)
            {
                passwordBox.GotFocus += OnFieldSelectPWAllFocus;
                passwordBox.GotMouseCapture += OnFieldSelectPWAllFocus;
            }
            foreach (var textBox in TextboxesNum)
            {
                textBox.TextChanged += OnSettingsValidator;
                textBox.GotFocus += OnFieldSelectAllFocus;
                textBox.GotMouseCapture += OnFieldSelectAllFocus;
            }

            Freq_TB.TextChanged -= OnSettingsValidator;
            Freq_TB.TextChanged += OnSettingsValidator4;

            Log_TB.TextChanged -= OnSettingsValidator;
            Log_TB.TextChanged += OnSettingsValidator4;
            //BindPassword(true);
        }

        //Control methods
        void SelectAll(object sender, bool isPW = false)
        {
            //for event, use On GotMouseCapture and GotFocus
            if (!isPW)
            {
                var s = sender as TextBox;
                if (s != null)
                {
                    s.Focus();
                    s.SelectAll();
                }
            }
            else
            {
                var s = sender as PasswordBox;
                if (s != null)
                {
                    s.Focus();
                    s.SelectAll();
                }
            }
        }
        //void BindPassword(bool isDataGet)
        //{
        //    if (isDataGet)
        //    {
        //        Password_TB.Password = BindedConfig.Password;
        //    }
        //    else
        //    {
        //        BindedConfig.Password = Password_TB.Password;
        //        Console.WriteLine(MirrorAll.configs.Password);
        //        save();
        //    }
        //}
        void SelectionToFields()
        {
            if (Mirror_list.SelectedItems.Count > 1)
            {
                return;
            }
            try
            {
                var c = ProfileModel.GetByTitle(MirrorAll.configs.Profiles, Mirror_list.SelectedItems[0].ToString());
                email_TB.Text = c.email;
                pw_TB.Password = c.password;
                wb_TB.Text = c.webmail;

                email_TB1.Text = c.email1;
                pw_TB1.Password = c.password1;
                wb_TB1.Text = c.webmail1;
            }
            catch
            {

            }
        }
        void AllTextCursorToEnd()
        {
            if (!_MALoaded) return;
            foreach (var textBox in TextBoxes)
            {
                TextCursorToEnd(textBox);
            }
            foreach (var textBox in TextboxesNum)
            {
                TextCursorToEnd(textBox);
            }
        }
        void TextCursorToEnd(TextBox t)
        {
            t.CaretIndex = t.Text.Length;
            t.ScrollToEnd();
        }
        void LastConfiger(bool commit) //use for rollback if validation fails
        {
            if (LastConfig == null)
            {
                LastConfig = new ConfigModel();
            }
            if (commit)
            {
                Utilities.Data.PropertyCopy(ref MirrorAll.configs, ref LastConfig);
            }
            else
            {
                Utilities.Data.PropertyCopy(ref LastConfig, ref MirrorAll.configs);
            }
        }

        //Events  
        void OnFieldSelectAllFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender);
        }
        void OnFieldSelectPWAllFocus(object sender, RoutedEventArgs e)
        {
            SelectAll(sender, true);
        }
        void OnTextboxChange(object sender, TextChangedEventArgs e)
        {
            save();
        }
        //void OnPasswordKey(object sender, KeyEventArgs e)
        //{
        //    BindPassword(false);
        //}
        void OnSettingsValidator(object sender, TextChangedEventArgs e)
        {
            NumericValidator(sender, 36, 2);
        }
        void OnSettingsValidator4(object sender, TextChangedEventArgs e)
        {
            NumericValidator(sender, 99999, 4);
        }
        void OnRealTimeChecker(object sender, RoutedEventArgs e)
        {
            CheckBoxValidator();
        }

        //speical events
        private void email_TB_LostFocus(object sender, RoutedEventArgs e)
        {
            AutoFillWebmail(email_TB, wb_TB);
        }
        private void email_TB1_LostFocus(object sender, RoutedEventArgs e)
        {
            AutoFillWebmail(email_TB1, wb_TB1);
        }
        private void wb_TB1_TextChanged(object sender, TextChangedEventArgs e)
        {
            var TB = sender as TextBox;
            ValidateURL(TB);
        }
        private void wb_TB_TextChanged(object sender, TextChangedEventArgs e)
        {
            var TB = sender as TextBox;
            ValidateURL(TB);
        }
        private void Mirror_list_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Console.WriteLine("Selection");
            SelectionToFields();
        }

        //validators
        void AutoFillWebmail(TextBox emailTB, TextBox webmail)
        {
            if (webmail != null)
            {
                if (Utilities.StringValidator.IsEmail(emailTB.Text))
                {
                    webmail.Text = "webmail." + Utilities.StringFormatter.GetDomainFromEmail(emailTB.Text, true);
                    emailTB.Background = Brushes.White;
                }
                else
                {
                    emailTB.Background = Brushes.PaleVioletRed;
                }

            }
        }
        void ValidateURL(TextBox TB)
        {
            if (Utilities.StringValidator.IsURL(TB.Text))
            {
                TB.Background = Brushes.White;
                Console.WriteLine("valid url");
            }
            else
            {
                TB.Background = Brushes.PaleVioletRed;
            }
        }
        void CheckBoxValidator()
        {
            if (!_MALoaded) return;
            if (Realtime_CB.IsChecked.Value)
            {
                Freq_TB.IsEnabled = false;
            }
            else
            {
                Freq_TB.IsEnabled = true;
            }
            save();
        }
        void NumericValidator(object TextboxField, int valueLimit, int limit = 3)
        {
            if (!_MALoaded) return;
            var field = TextboxField as TextBox;
            var valid = Utilities.StringValidator.OnlyNumber(field.Text, limit, true);

            if (!valid || Utilities.StringFormatter.stringToInt(field.Text) > valueLimit) //revert from save
            {
                Console.WriteLine("invalid");
                LastConfiger(false);
                AllTextCursorToEnd();
            }
            if (valid && field.Text != "") //commit and save
            {
                LastConfiger(true);
                save();
            }
        }
        #endregion
    }
}
