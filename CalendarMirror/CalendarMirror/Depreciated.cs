﻿namespace CalendarMirror
{
    #region DepreciatedEntity
    //[Serializable]
    //public class TargetEntity : IComparison
    //{
    //    [Key]
    //    public int Index { get; set; }

    //    //public string BodyHTML { get; set; }
    //    public string BodyHTML { get; set; }
    //    public int BodyHash { get; set; }
    //    public int dbTag { get; set; }
    //    public string ItemId { get; set; }
    //    public string Subject { get; set; }
    //    public DateTime End { get; set; }
    //    public DateTime Start { get; set; }
    //    public byte[] Catagories { get; set; }
    //    public Importance Importance { get; set; }
    //    public string Location { get; set; }
    //    public bool IsNew { get; set; }
    //    public bool IsSafe { get; set; }
    //    public string Body { get; set; }

    //    public StringList GetCatagories()
    //    {
    //        var ls = Utilities.Data.ByteArrayToObject(Catagories) as string[];
    //        var ls1 = new StringList();
    //        for (var i = 0; i < ls.Length; i++)
    //        {
    //            ls1.Add(ls[i]);
    //        }
    //        return ls1;
    //    }
    //    public void SetCatagories(StringList List)
    //    {
    //        var ls = new string[List.Count];
    //        for (var i = 0; i < List.Count; i++)
    //        {
    //            ls[i] = List[i];
    //        }
    //        Catagories = Utilities.Data.ObjectToByteArray(ls);
    //    }

    //    public void SetItemId(ItemId id)
    //    {
    //        ItemId = id.ToString();
    //    }
    //    public ItemId GetItemId()
    //    {
    //        return new ItemId(ItemId);
    //    }

    //    public void SetBody(MessageBody MBody)
    //    {
    //        BodyHTML = Utilities.StringFormatter.HTMLBodyExtract(MBody);
    //        BodyHash = BodyHTML.GetHashCode();
    //        Body = MBody.Text;
    //    }
    //    public MessageBody GetBody()
    //    {
    //        return new MessageBody(Body);
    //    }
    //}
    //[Serializable]
    //public class SourceEntity : IComparison
    //{
    //    [Key]
    //    public int Index { get; set; }

    //    public string BodyHTML { get; set; }
    //    public int BodyHash { get; set; }
    //    public int dbTag { get; set; }
    //    public string ItemId { get; set; }
    //    public string Subject { get; set; }
    //    public DateTime End { get; set; }
    //    public DateTime Start { get; set; }
    //    public byte[] Catagories { get; set; }
    //    public Importance Importance { get; set; }
    //    public string Location { get; set; }
    //    public bool IsNew { get; set; }
    //    public bool IsSafe { get; set; }
    //    public string Body { get; set; }

    //    public StringList GetCatagories()
    //    {
    //        var ls = Utilities.Data.ByteArrayToObject(Catagories) as string[];
    //        var ls1 = new StringList();
    //        for (var i = 0; i < ls.Length; i++)
    //        {
    //            ls1.Add(ls[i]);
    //        }
    //        return ls1;
    //    }
    //    public void SetCatagories(StringList List)
    //    {
    //        var ls = new string[List.Count];
    //        for (var i = 0; i < List.Count; i++)
    //        {
    //            ls[i] = List[i];
    //        }
    //        Catagories = Utilities.Data.ObjectToByteArray(ls);
    //    }

    //    public void SetItemId(ItemId id)
    //    {
    //        ItemId = id.ToString();
    //    }
    //    public ItemId GetItemId()
    //    {
    //        return new ItemId(ItemId);
    //    }

    //    public void SetBody(MessageBody MBody)
    //    {
    //        BodyHTML = Utilities.StringFormatter.HTMLBodyExtract(MBody);
    //        BodyHash = BodyHTML.GetHashCode();
    //        Body = MBody.Text;
    //    }
    //    public MessageBody GetBody()
    //    {
    //        return new MessageBody(Body);
    //    }
    //}


    #endregion


    #region DepreciatedModel

    //AppointmentSchema.Subject, 
    //AppointmentSchema.End, 
    //AppointmentSchema.Start, 
    //AppointmentSchema.Id, 
    //AppointmentSchema.Body,
    //AppointmentSchema.Categories,
    //AppointmentSchema.Importance,
    //AppointmentSchema.TextBody,
    //AppointmentSchema.Location,
    //AppointmentSchema.DateTimeCreated
    //public class ComparisonModelX
    //{
    //    [Key]
    //    public int Index { get; set; }
    //    public string Subject { get; set; }
    //    public DateTime End { get; set; }
    //    public DateTime Start { get; set; }
    //    public StringList Catagories { get; set; }
    //    public Importance Importance { get; set; }
    //    public string Location { get; set; }

    //    //bottom properties not use for comparison
    //    public MessageBody body { get; set; }

    //    public ItemId ID { get; set; } //not use for creating appointments

    //    public static ComparisonModelX GetDefault()
    //    {
    //        var c = new ComparisonModelX();
    //        c.Subject = "Sub1";
    //        c.End = DateTime.Now.AddHours(1);
    //        c.Start = DateTime.Now;
    //        c.Catagories = new StringList();
    //        c.Importance = Importance.Low;
    //        c.Location = "Location";
    //        c.Index = 123;
    //        return c;
    //    }



    //}
    #endregion

    #region DepreciatedEF

    //public class EF
    //{
    //    private ExchangeService Target;
    //    private ExchangeService Source;
    //    private List<Appointment> CalendarAdd;
    //    private int ProfileIndex; //use to work with specific profile on same table
    //    public EF(ExchangeService source, ExchangeService target, int profileIndex)
    //    {
    //        Target = target;
    //        Source = source;
    //        ProfileIndex = profileIndex;
    //    }

    //    public string Reset()
    //    {
    //        CalendarAdd = new List<Appointment>();
    //        return ClearSelf();
    //    }

    //    //was leaking
    //    public void Add_Debug(FindItemsResults<Appointment> calendar)
    //    {
    //        var Profile = Source;
    //        var itr = 0;
    //        while (true)
    //        {
    //            ClearSelf();
    //            using (var c = new ComparisonContext())
    //            {
    //                if (calendar == null) return;
    //                var T = new List<SourceEntity>();
    //                foreach (var i in calendar)
    //                {
    //                    Console.WriteLine("Iterations " + itr);
    //                    itr++;
    //                    var Ti = Comparison.AppointmentToIComparisonS(i, Calendar.GetBodyByID(Profile, i.Id),
    //                        ProfileIndex);
    //                    T.Add(Ti);
    //                }
    //                foreach (var entity in T)
    //                {
    //                    c.SEntity.Add(entity);
    //                }
    //                c.SaveChanges();
    //            }
    //        }
    //    }

    //    public void AddSource(FindItemsResults<Appointment> calendar)
    //    {
    //        if (calendar == null) return;
    //        var Profile = Source;
    //        var eList = new List<SourceEntity>();
    //        var T = new List<Task<SourceEntity>>();
    //        foreach (var i in calendar)
    //        {
    //            //var doc = new HtmlDocument();
    //            //var st = Calendar.GetBodyByID(Profile, i.Id).Text;
    //            //var doc1 = doc.DetectEncodingHtml(st);
    //            //Console.WriteLine(doc1);
    //            T.Add(Task.Run(() => Comparison.AppointmentToIComparisonS(i, Calendar.GetBodyByID(Profile, i.Id), ProfileIndex)));
    //        }
    //        Task.WaitAll(T.ToArray());
    //        foreach (var task in T)
    //        {
    //            eList.Add(task.Result);
    //        }
    //        foreach (var task in T)
    //        {
    //            task.Dispose();
    //        }
    //        using (var c = new ComparisonContext())
    //        {
    //            foreach (var e in eList)
    //            {
    //                c.SEntity.Add(e);
    //            }
    //            c.SaveChanges();
    //        }
    //    }
    //    public void AddTarget(FindItemsResults<Appointment> calendar)
    //    {
    //        if (calendar == null) return;
    //        var Profile = Target;
    //        var eList = new List<TargetEntity>();
    //        var T = new List<Task<TargetEntity>>();
    //        foreach (var i in calendar)
    //        {
    //            T.Add(Task.Run(() => Comparison.AppointmentToIComparisonT(i, Calendar.GetBodyByID(Profile, i.Id), ProfileIndex)));
    //        }
    //        Task.WaitAll(T.ToArray());
    //        foreach (var task in T)
    //        {
    //            eList.Add(task.Result);
    //        }
    //        foreach (var task in T)
    //        {
    //            task.Dispose();
    //        }
    //        using (var c = new ComparisonContext())
    //        {
    //            foreach (var e in eList)
    //            {
    //                c.TEntity.Add(e);
    //            }
    //            c.SaveChanges();
    //        }
    //    }

    //    public Tuple<string, int> Compare()
    //    {
    //        var error = "";
    //        var changes = 0;

    //        var AddTEntity = new List<TargetEntity>();
    //        using (var c = new ComparisonContext())
    //        {
    //            //Get source and target db
    //            //Removing this and disable deduplication() can cause duplication => where j.dbTag == ProfileIndex
    //            //might be useful for debugggin
    //            var source = from j in c.SEntity where j.dbTag == ProfileIndex select j;
    //            var target = from j in c.TEntity where j.dbTag == ProfileIndex select j;

    //            //find all target that already exist in source
    //            var TargetMatchSource = target.Where(t => source.Any(s =>
    //                s.Catagories == t.Catagories &&
    //                s.Subject == t.Subject &&
    //                s.Start == t.Start &&
    //                s.End == t.End &&
    //                s.Importance == t.Importance &&
    //                s.Location == t.Location &&
    //                s.BodyHash == t.BodyHash
    //            ));

    //            var SourceMatchTarget = source.Where(s => target.Any(t =>
    //                t.Catagories == s.Catagories &&
    //                t.Subject == s.Subject &&
    //                t.Start == s.Start &&
    //                t.End == s.End &&
    //                t.Importance == s.Importance &&
    //                t.Location == s.Location &&
    //                t.BodyHash == s.BodyHash
    //            ));



    //            var SourceNotMatchTarget = source.Except(SourceMatchTarget);

    //            //commit settings for those target not being deleted
    //            var save = false;
    //            try
    //            {
    //                foreach (var t in TargetMatchSource)
    //                {
    //                    t.IsSafe = true;
    //                    if (!save) save = true;
    //                }
    //            }
    //            catch
    //            {
    //                error = "Exception at compare when foreach TargetMatchSource.";
    //                save = false;
    //            }

    //            //add new source that are not in target
    //            try
    //            {
    //                foreach (var newest in SourceNotMatchTarget)
    //                {
    //                    var AsTEntity = Comparison.SourceEntityToTargetEntity(newest);
    //                    AddTEntity.Add(AsTEntity);
    //                    CalendarAdd.Add(Comparison.TargetEntityToAppointment(AsTEntity, Target));
    //                }
    //            }
    //            catch
    //            {
    //                error = error + ", Exception at compare when foreach SourceNotMatchTarget.";
    //            }
    //            if (save) c.SaveChanges();
    //        }

    //        changes = AddTEntity.Count();
    //        using (var c = new ComparisonContext())
    //        {
    //            foreach (var AsTEntity in AddTEntity)
    //            {
    //                c.TEntity.Add(AsTEntity);
    //            }
    //            c.SaveChanges();
    //        }
    //        return Tuple.Create(error, changes);
    //    }

    //    public string Deduplicate()
    //    {
    //        Console.WriteLine("Deduplicating");
    //        using (var c = new ComparisonContext())
    //        {
    //            var error = "";
    //            var target = from j in c.TEntity where j.dbTag == ProfileIndex && j.IsSafe select j;
    //            var Grouping = target.GroupBy(x => new { x.Catagories, x.Start, x.End, x.Subject, x.Location, x.Importance, x.BodyHash });
    //            var anyGroup = false;
    //            try
    //            {
    //                foreach (var group in Grouping)
    //                {
    //                    var marked = false;
    //                    Console.WriteLine("Duplication found");
    //                    foreach (var i in group)//each of these group unique, but their items is all the same base on gets parameter
    //                    {
    //                        if (!marked && i.ItemId.Substring(0, 3) != "NULL")
    //                        {
    //                            i.IsSafe = true;
    //                            marked = true;
    //                            continue;
    //                        }
    //                        i.IsSafe = false;
    //                    }
    //                }
    //                c.SaveChanges();
    //            }
    //            catch
    //            {
    //                error = "Error in foreach Grouping";
    //            }
    //            return error;
    //        }
    //    }

    //    public Tuple<List<Result>, string> TargetUnsafeDelete()
    //    {
    //        var error = "";
    //        var DelExist = false;
    //        var results = new List<Result>();
    //        var T = new List<Task<Result>>();
    //        var info = new List<Tuple<string, string>>();
    //        var cDel = new Calendar();
    //        using (var c = new ComparisonContext())
    //        {
    //            var target = from j in c.TEntity where j.dbTag == ProfileIndex && !j.IsSafe select j;
    //            try
    //            {
    //                foreach (var del in target)
    //                {
    //                    info.Add(Tuple.Create(del.Subject, del.ItemId));
    //                    DelExist = true;
    //                }
    //            }
    //            catch
    //            {
    //                error = "Error in target add to delete info";
    //            }
    //        }
    //        if (!DelExist) return Tuple.Create(results, error);
    //        foreach (var i in info)
    //        {
    //            T.Add(Task.Run(() => cDel.AsyncDeleteCalenderItem(Target, i.Item1, i.Item2)));
    //        }
    //        Task.WaitAll(T.ToArray());
    //        foreach (var task in T)
    //        {
    //            results.Add(task.Result);
    //        }
    //        foreach (var task in T)
    //        {
    //            task.Dispose();
    //        }
    //        return Tuple.Create(results, error);
    //    }

    //    public List<Result> TargetAdd()
    //    {
    //        var results = new List<Result>();
    //        var T = new List<Task<Result>>();
    //        var cDel = new Calendar();
    //        var anyCalendarAdd = CalendarAdd.Count() > 0;
    //        if (anyCalendarAdd)
    //        {
    //            foreach (var add in CalendarAdd)
    //            {
    //                T.Add(Task.Run(() => cDel.AsyncAddCalenderItem(add)));
    //            }
    //            Task.WaitAll(T.ToArray());
    //            foreach (var task in T)
    //            {
    //                results.Add(task.Result);
    //            }
    //            foreach (var task in T)
    //            {
    //                task.Dispose();
    //            }
    //        }
    //        return results;
    //    }

    //    public string ClearSelf()
    //    {
    //        var resultMsg = "";
    //        int result = 0;
    //        using (var c = new ComparisonContext())
    //        {
    //            var source = from j in c.SEntity where j.dbTag == ProfileIndex select j;
    //            try
    //            {
    //                foreach (var t in source)
    //                {
    //                    c.SEntity.Remove(t);
    //                }
    //                c.SaveChanges();
    //            }
    //            catch
    //            {
    //                result = 1;
    //            }
    //        }
    //        using (var c = new ComparisonContext())
    //        {
    //            var target = from j in c.TEntity where j.dbTag == ProfileIndex select j;
    //            try
    //            {
    //                foreach (var t in target)
    //                {
    //                    c.TEntity.Remove(t);
    //                }
    //                c.SaveChanges();
    //            }
    //            catch
    //            {
    //                result = 2 + result;
    //            }
    //        }
    //        switch (result)
    //        {
    //            case 0: //no errors
    //                break;
    //            case 1: //errors in S del
    //                resultMsg = "There was an error deleting TEntity.";
    //                break;
    //            case 2:
    //                resultMsg = "There was an error deleting SEntity.";
    //                break;
    //            case 3:
    //                resultMsg = "There was an error deleting SEntity and TEntity.";
    //                break;
    //        }
    //        return resultMsg;
    //    }
    //    public static void ClearTable()
    //    {
    //        using (var c = new ComparisonContext())
    //        {
    //            var tableName = "SourceEntities";
    //            Utilities.EF.EmptyResetTableLocalDB(c, tableName);
    //            var tableName2 = "TargetEntities";
    //            Utilities.EF.EmptyResetTableLocalDB(c, tableName2);
    //        }
    //    }
    //}

    #endregion
}
