﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Microsoft.Exchange.WebServices.Data;
using Task = System.Threading.Tasks.Task;

namespace CalendarMirror
{
    public class Mirror
    {
        private FindItemsResults<Appointment> SourceCalender { get; set; }
        private FindItemsResults<Appointment> TargetCalender { get; set; }
        private ExchangeService source { get; set; }
        private ExchangeService target { get; set; }
        public Task Run;
        public ProfileModel Profile;
        public CancellationTokenSource tokenSource;
        private readonly Comparator _comparator;
        public int CriticalChanges;
        public int RanTime = 0;
        private int PingFailed = 0;

        public Mirror(ExchangeService source, ExchangeService target, ProfileModel Profile)
        {  
            this.source = source;
            this.target = target;
            this.Profile = Profile;
            _comparator = new Comparator(source, target);
        }

        public void log(string Text, bool isError)
        {
            if (isError)
            {
                MirrorAll.Logger.L.Error(Profile.Title + ","+Text);
            }
            else
            {
                MirrorAll.Logger.L.Info(Profile.Title + "," + Text);
            }
        }
        
        public void Start()
        {
            RanTime++;
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            if (MirrorAll.IsFirstRun) //allows no logs if no changes occures
            {
                log("Service started", false);
            }
            #region Prepare  
            CriticalChanges = 0;
            _comparator.Reset();
            tokenSource = new CancellationTokenSource();
            #endregion
            Run = Task.Run(() =>
            {
                Predicate<bool> CheckCancel = (bool x) => {
                    if (tokenSource.IsCancellationRequested)
                    {
                        if(CriticalChanges > 0) log("Mirroring service has been stopped or is restarting.", false);
                        return true;
                    }
                    return false;
                };

                if (!PingIp()) return;

                #region Processes
                if (CheckCancel.Invoke(true)) return;
                GetCalendars();
                if (CheckCancel.Invoke(true)) return;
                CompareData();
                if (CheckCancel.Invoke(true)) return;
                CommitChanges();
                if (CheckCancel.Invoke(true)) return;
                #endregion

                #region EndProcesses
                var secondsElapsed = (float)stopwatch.ElapsedMilliseconds / 1000;
                if (CriticalChanges > 0)
                {
                    var msg = "Mirroring completed with " + CriticalChanges + " changes " +
                              "with Process Time (Sec) " + secondsElapsed;
                    log(msg, false); 
                }
                if (!MirrorAll.configs.RealTime)
                {
                    Console.WriteLine("Resting: " + Profile.Title);
                    log("Resting for " + MirrorAll.configs.Frequency + " minutes", false);
                    Thread.Sleep(MirrorAll.configs.Frequency * 60000);
                }
                #endregion

            }, tokenSource.Token);
        }

        private bool PingIp()
        {
            //Gets the correct IP if more than one IP exist for a particular webmail URL for first run
            if (RanTime < 2) //RanTime starts at 1
            {
                Profile.webmailIP = Utilities.Network.ResolveURLExternally(Profile.webmail);
                Profile.webmailIP1 = Utilities.Network.ResolveURLExternally(Profile.webmail1);
                if (Profile.webmailIP == null || Profile.webmailIP1 == null)
                {
                    PingFailed++;
                    if (PingFailed > 5)
                    {
                        PingFailed = 0;
                        log("Source or Target IP cannot be ping.", true);
                    }
                    Thread.Sleep(5000);
                    return false;
                }
                Console.WriteLine("IP resolved successfully");
                return true;
            }
            return true;
        }
        private void GetCalendars()
        {
            var GetSourceAndTarget = new List<Task>();
            var t1 = Task.Run(() =>
            {
                SourceCalender = Calendar.GetCalendar(source, MirrorAll.configs.BackMonths, MirrorAll.configs.ForwardMonths);
                Console.WriteLine("Adding source");
                _comparator.AddCalendarSource(SourceCalender);
            });
            var t2 = Task.Run(() =>
            {
                TargetCalender = Calendar.GetCalendar(target, MirrorAll.configs.BackMonths, MirrorAll.configs.ForwardMonths);
                Console.WriteLine("Adding target");
                _comparator.AddCalendarTarget(TargetCalender);
            });
            GetSourceAndTarget.Add(t1);
            GetSourceAndTarget.Add(t2);
            Task.WaitAll(GetSourceAndTarget.ToArray());
            Console.WriteLine("all calendar retrieved");
        }
        private void CompareData()
        {
            var ret = _comparator.Deduplicate();
            if (ret != "") log(ret, true);
            Console.WriteLine("Deduped");
            var ret2 = _comparator.Compare();
            if (ret2 != "") log(ret2, true);
            Console.WriteLine("compare");
            //var ret = _databaseProcessor.Compare();
            //if (ret.Item1 != "")
            //{
            //    log(ret.Item1, true);
            //}
            //var ret2 = _databaseProcessor.Deduplicate();
            //if (ret2 != "")
            //{
            //    log(ret2, true);
            //}
        }
        private void CommitChanges()
        {
            //var timer = new Utilities.Result("Commiting db").StartTimer();
            //var Del = Task.Run(()=>_databaseProcessor.TargetUnsafeDelete());
            //var Add = Task.Run(() => _databaseProcessor.TargetAdd()); 

            var Del = Task.Run(()=> _comparator.Discard());
            Console.WriteLine("discarded");
            var Add = Task.Run(() => _comparator.Add());
            Console.WriteLine("added");
            var TList = new Task[2];
            TList[0] = Del;
            TList[1] = Add;
            Task.WaitAll(TList);
            //timer.DisplayResults().ToConsoleTimer();
            foreach (var result in Del.Result.Item1)
            {
                if (result.IsSuccess)
                {
                    log("Removed: " + result.subject, false);
                }
                else
                {
                    log("Removed: " + result.subject, true);
                }
                result.DisplayResults().ToConsole(); 
            } 
            foreach (var result in Add.Result)
            {
                if (result.IsSuccess)
                {
                    log("Added: " + result.subject, false);
                }
                else
                {
                    log("Added: " + result.subject, true);
                }
                result.DisplayResults().ToConsole();
                CriticalChanges++;
            }
            CriticalChanges = Add.Result.Count() + Del.Result.Item1.Count;
            Console.WriteLine("committing completed");
        }

    }
}
