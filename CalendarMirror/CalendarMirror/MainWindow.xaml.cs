﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Utilities;

namespace CalendarMirror
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    ///  
    public partial class MainWindow : Window
    {
        private List<TextBox> TextBoxes;
        private List<TextBox> TextboxesNum;
        private List<PasswordBox> PasswordBoxes;
        public MirrorAll MA { get; set; }
        public ConfigModel BindedConfig { get; set; }
        public ConfigModel LastConfig;
        private BindingList<string> mirrorNames;
        private bool _MALoaded;
        public MainWindow()
        {
            InitializeComponent(); 
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Init.Start();
            MA = new MirrorAll();
            mirrorNames = new BindingList<string>(MirrorAll.configs.Names());
            Mirror_list.ItemsSource = mirrorNames;
               
            if (Mirror_list.Items.Count < 1)
            {
                email_TB.Text = "[SOURCE EMAIL]";
                pw_TB.Password = "";
                wb_TB.Text = "";

                email_TB1.Text = "[TARGET EMAIL]";
                pw_TB1.Password = "";
                wb_TB1.Text = "";
            }
            ScrollToBottom();
            DataContext = this;
            BindedConfig = MirrorAll.configs;
            AllTextCursorToEnd(); 
            LastConfiger(true);
            _MALoaded = true;
            CheckBoxValidator(); //just for realtime checkbox

            //Select first on profile list and put on fields
            Mirror_list.SelectedIndex = 0;
            SelectionToFields();

            DisableServiceControls();
            CheckInstalled();

            var DebugVisibility = Visibility.Hidden; //change this one variable to toggle debugging controls
            debug.Visibility = DebugVisibility;
            debug_Restart.Visibility = DebugVisibility;
            debug_Stop.Visibility = DebugVisibility;
            Service_lbl.Content = "Checking Service...";

            InitiControls();
        }

        #region AddRemove

        void ControlIdle(bool isProcessing)
        {
            Toggle_BT.IsEnabled = !isProcessing;
            Remove_BT.IsEnabled = !isProcessing;
            Add_BT.IsEnabled = !isProcessing;
        }
        private void Toggle_BT_Click(object sender, RoutedEventArgs e)
        {
            ControlIdle(true);
            ToggleSelection(); save();
            ControlIdle(false);
        }
        private void Remove_BT_Click(object sender, RoutedEventArgs e)
        {
            ControlIdle(true);
            RemoveList();
            save();
            ControlIdle(false);
        }

        private void Add_BT_Click(object sender, RoutedEventArgs e)
        {
            ControlIdle(true);
            AddList();
            save();
            ControlIdle(false);
        }


        void ToggleSelection()
        {
            if (Mirror_list.SelectedItems.Count < 1) return;
            ProfileModel toggleThis = ProfileModel.GetByTitle(MirrorAll.configs.Profiles, Mirror_list.SelectedItems[0].ToString());

            //check if another profile is already active with the same target
            foreach (var p in MirrorAll.configs.Profiles)
            {
                if (p.email1 == email_TB1.Text)
                {
                    if (p.isEnabled && !toggleThis.isEnabled)
                    {
                        System.Windows.MessageBox.Show("You cannot have more than one of the same target active at once: " + p.email1);
                        return;
                    }   
                }
            }

            var originialTitle = Mirror_list.SelectedItems[0].ToString();
            var newTitle = "";
            if (toggleThis.isEnabled)
            {
                newTitle = GenerateTitle(toggleThis.email, toggleThis.email1, false);
                toggleThis.isEnabled = false;
            }
            else
            {
                newTitle = GenerateTitle(toggleThis.email, toggleThis.email1, true);
                toggleThis.isEnabled = true;
            }
            for (var i = 0; i < mirrorNames.Count; i++)
            {
                if (mirrorNames[i] == originialTitle)
                {
                    mirrorNames[i] = newTitle;
                    break;
                }
            }
            toggleThis.Title = newTitle;
        }
        void AddList()
        {
            var c = new ProfileModel();
            var title = GenerateTitle(email_TB.Text, email_TB1.Text, true);

            //check for duplicate profile
            if (mirrorNames.Contains(title))
            {
                System.Windows.MessageBox.Show("This mirror may already exist.");
                return;
            }

            //check for another profile with same target
            foreach (var p in MirrorAll.configs.Profiles)
            {
                if (p.email1 == email_TB1.Text)
                {
                    if (p.isEnabled)
                    {
                        System.Windows.MessageBox.Show("You cannot have more than one of the same target active at once: " + p.email1);
                        title = GenerateTitle(email_TB.Text, email_TB1.Text, false);
                    }
                }
            }
            var sSource = Exchange.TestService(email_TB.Text, pw_TB.Password, wb_TB.Text);
            var tSource = Exchange.TestService(email_TB1.Text, pw_TB1.Password, wb_TB1.Text);

            if (!sSource || !tSource)
            {
                MessageBox.Show(string.Format(
                    "A connection has failed: {0}Source Connection: {1}{2}Target Connection: {3}", Environment.NewLine,
                    sSource, Environment.NewLine, tSource));
                return;
            }

            c.Title = title;
            c.isEnabled = true;
            c.email = email_TB.Text;
            c.password = pw_TB.Password;
            c.webmail = wb_TB.Text;

            c.email1 = email_TB1.Text;
            c.password1 = pw_TB1.Password;
            c.webmail1 = wb_TB1.Text;

            MirrorAll.configs.Profiles.Add(c);

            mirrorNames.Add(c.Title);
        }

        void RemoveList()
        {
            var toRemove = new List<string>();
            foreach (var s in Mirror_list.SelectedItems)
            {
                toRemove.Add(s.ToString());
            }
            foreach (var s in toRemove)
            {
                ProfileModel.RemoveByTitle(MirrorAll.configs.Profiles, s);
                mirrorNames.Remove(s);
            }
        }

        #endregion


        #region Common
        string GenerateTitle(string source, string mirror, bool isActive)
        {
            var tag = "";
            if (isActive)
            {
                tag = " ACTIVE";
            }
            else
            {
                tag = " INACTIVE";
            }
            return source + " => " + mirror + tag;
        }

        void save()
        {
            if (!_MALoaded) return;
            Console.WriteLine("saved");
            Console.WriteLine("checkbox: " + MirrorAll.configs.RealTime);       
            MA.DataManager.save(MirrorAll.configs);
            Console.WriteLine(MirrorAll.configs.ForwardMonths);
            ScrollToBottom();
        }

        void ScrollToBottom()
        {
            if (Mirror_list.Items.Count < 1) return;
            Mirror_list.ScrollIntoView(Mirror_list.Items[Mirror_list.Items.Count - 1]);
        }

        private async void debug_Click(object sender, RoutedEventArgs e)
        {
            //Utilities.Network.Ping("webmail.fensterstock.com", -1);
            await Task.Run(()=>MA.Start());
        }
        private async void debug_Stop_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(()=>MA.Stop());
        }
        private async void debug_Restart_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(() => MA.Restart());
        }
        #endregion


    }
}
