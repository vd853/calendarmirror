﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Utilities;

namespace CalendarMirror
{
    public partial class MainWindow : Window
    {
        #region Service Controllers
        Utilities.Service Service = new Utilities.Service();
        private bool ServiceWait; //use for awaiting
        public const string ServiceName = "Calendar Mirror";
        public const string ServicePIDName = "CalendarMirror";
        private async void install_Click(object sender, RoutedEventArgs e)
        {
            DisableServiceControls();
            Service_lbl.Content = "Installing Service...";
            var InstalledError = false;
            await Task.Run(() =>
            {
                try
                {
                    if (Processes.ProcessExist(ServicePIDName, Process.GetCurrentProcess()))
                    {
                        throw new Exception();
                    }
                    Service.TSServiceInstaller("CalendarMirror.exe", ServiceName);
                }
                catch
                {
                    MessageBox.Show("Error installing the service. You must have administrator privilage and another service or process cannot be running.");
                    InstalledError = true;
                }
            });
            if (InstalledError)
            {
                CheckInstalled();
                return;
            }
            await Task.Run(() =>
            {
                while (Service.ServiceStatus(ServiceName) != "running")
                {
                    Thread.Sleep(500);
                }
            });
            CheckInstalled();
        }
        private async void uinstall_Click(object sender, RoutedEventArgs e)
        {
            DisableServiceControls();
            ServiceWait = true;
            Service_lbl.Content = "Service is stopping for uninstall";
            ServiceMode.isUninstalledTriggered = true;
            if (Service.ServiceStatus(ServiceName) != "stopped")
            {
                Service.ServiceChangeHandler += CheckServiceMode;
                Service.ToggleServiceByName(ServiceName, Service.mode.stop, 120000); //give it 2min to stop
                Console.WriteLine("stopping");
                await Task.Run(() =>
                {
                    while (ServiceWait)
                    {
                        Console.WriteLine("waitning to stop");
                        Thread.Sleep(250);
                    }
                });
            }
            await Task.Run(() =>
            {
                try
                {
                    Service.TSServiceInstaller("CalendarMirror.exe", ServiceName, Process.GetCurrentProcess(), ServicePIDName, true);
                }
                catch
                {
                    MessageBox.Show("Error uninstalling the service. You must have administrator privilage.");
                }

            });
            CheckInstalled();
        }
        void CheckInstalled()
        {
            Service.ServiceChangeHandler += CheckServiceMode;
            Service.ToggleServiceByName(ServiceName, Service.mode.none);
        }
        void CheckServiceMode(object o, Utilities.Service.ServiceEvent e)
        {
            switch (e.status)
            {
                case "stopped":
                    Start.IsEnabled = true;
                    Restart.IsEnabled = false;
                    Stop.IsEnabled = false;
                    ServiceWait = false;
                    break;
                case "running":
                    Start.IsEnabled = false;
                    Restart.IsEnabled = true;
                    Stop.IsEnabled = true;
                    break;
                case "stopping":
                    DisableServiceControls();
                    break;
            }
            if (e.installed)
            {
                install.IsEnabled = false;
                uinstall.IsEnabled = true;
            }
            else
            {
                DisableServiceControls();
                install.IsEnabled = true;
            }
            var status = "Service is " + e.status;
            Service_lbl.Content = status;
            Service.ServiceChangeHandler -= CheckServiceMode;
        }
        void DisableServiceControls(bool NotDisabled = false)
        {
            install.IsEnabled = NotDisabled;
            uinstall.IsEnabled = NotDisabled;
            Start.IsEnabled = NotDisabled;
            Restart.IsEnabled = NotDisabled;
            Stop.IsEnabled = NotDisabled;
        }
        private void Start_Click(object sender, RoutedEventArgs e)
        {
            DisableServiceControls();
            Service_lbl.Content = "Service is starting";
            Service.ServiceChangeHandler += CheckServiceMode;
            ServiceStart();
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            DisableServiceControls();
            Service_lbl.Content = "Service is stopping";
            Service.ServiceChangeHandler += CheckServiceMode;
            ServiceStart(false);
        }
        private async void Restart_Click(object sender, RoutedEventArgs e)
        {
            DisableServiceControls();
            ServiceWait = true;
            Service_lbl.Content = "Service is restarting";
            Service.ServiceChangeHandler += CheckServiceMode;
            ServiceStart(false);
            Console.WriteLine("stopping");
            await Task.Run(() =>
            {
                while (ServiceWait)
                {
                    Console.WriteLine("waitning to stop");
                    Thread.Sleep(250);
                }
            });
            Console.WriteLine("starting");
            Service_lbl.Content = "Service is starting";
            Service.ServiceChangeHandler += CheckServiceMode;
            ServiceStart();
        }

        void ServiceStart(bool Start = true)
        {
            if (!Start)
            {
                try
                {
                    Service.ToggleServiceByName(ServiceName, Service.mode.stop);
                }
                catch
                {
                    MessageBox.Show("Service is taking longer than usually to stop.");
                }
                return;
            }
            try
            {
                Service.ToggleServiceByName(ServiceName, Service.mode.start);
            }
            catch
            {
                MessageBox.Show("Service is taking longer than usually to startup.");
            }
        }

        
        #endregion
    }
}
