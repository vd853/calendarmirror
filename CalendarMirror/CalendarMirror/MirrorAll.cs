﻿using System;
using System.Collections.Generic;
using System.Threading;
using Utilities;
using Task = System.Threading.Tasks.Task;

namespace CalendarMirror
{
    public class MirrorAll
    {
        public static ConfigModel configs;
        public FileSerialization DataManager;
        public static List<Mirror> Mirrors;
        private const int RunLimit = 30; //numbers of time each mirror will run until global restart, divide this default by the number of profile being run

        private bool _mainStarted;
        public bool MainRunning;
        public Task MainTask;//, RestartTask;
        public static bool IsFirstRun = true;
        public static FileLogL4N Logger = null;
        public bool ProfileLess;
        public MirrorAll()
        {
            DataManager = new FileSerialization("data.data", ConfigModel.GetNew());
            Reload();
        }

        public void Reload()
        {
            configs = (ConfigModel)DataManager.load();
        }
        public void Start()
        {
            if (Logger == null)
            {
                Logger = new FileLogL4N(FileLogL4N.GenerateFileName(isCSV: true, folder: "Logs", upParent: 1), configs.LogKeep);
            }
            if (!_mainStarted)
            {
                Mirrors = new List<Mirror>();
                foreach (var p in configs.Profiles)
                {
                    if (!p.isEnabled)
                    {
                        Console.WriteLine("skip " + p.Title);
                        continue;
                    }
                    Console.WriteLine("Creating email for " + p.email + " and " + p.email1);
                    var getSource = Task.Run(()=>Exchange.GetService(p.email, p.password, p.webmail));
                    var getTarget = Task.Run(()=>Exchange.GetService(p.email1, p.password1, p.webmail1));
                    getSource.Wait(TimeSpan.FromSeconds(30));
                    getTarget.Wait(TimeSpan.FromSeconds(30));
                    var source = getSource.Result;
                    var target = getTarget.Result;
                    if (source == null || target == null)
                    {
                        Logger.L.Error(p.Title + " has failed to connect, profile will be skipped.");
                        //new Utilities.FileLog(p.Title + " has failed to connect, profile will be skipped.",
                        //    "Gobal error", LogName, true, "Logs");
                        continue;
                    }
                    var m = new Mirror(source, target, p);
                    Mirrors.Add(m);
                }
                if (Mirrors.Count < 1)
                {
                    Console.WriteLine("No profiles or active profiles to run");
                    ProfileLess = true;
                }
                else
                {
                    ProfileLess = false;
                } 
                foreach (var m in Mirrors)
                {
                    Console.WriteLine("First run: " + m.Profile.Title);
                    m.Start();
                }
                IsFirstRun = false;
                _mainStarted = true;
            }

            //init
            MainRunning = true;
            var queueLimit = Utilities.Mat.IntDivideRoundUp(RunLimit, Mirrors.Count);
            foreach (var m in Mirrors)
            {
                m.RanTime = 0;
            }

            //actual task
            MainTask = Task.Run(() =>
            { 
                while (MainRunning)
                {
                    //check if run times reached for restart
                    var Reset = false;
                    foreach (var m in Mirrors)
                    {
                        if (m.RanTime >= queueLimit)
                        {
                            Reset = true;
                        }
                        else
                        {
                            Reset = false;
                        }
                    }
                    if (Reset)
                    {
                        Console.WriteLine("Restarting expired rantime");
                        Task.Run(() => Restart());
                        return;
                    }
                    Thread.Sleep(GetNextRunDelay(ProfileLess));
                    //run mirror
                    foreach (var m in Mirrors)
                    {
                        if (m.Run.IsCompleted)
                        {
                            if (m.RanTime < queueLimit)
                            {
                                m.Start();
                                Console.WriteLine("Running a mirror: " + m.RanTime);
                            }
                        }
                    } 
                }
            });
        }

        public int GetNextRunDelay(bool profileLess = false) //slows down next cycle if no changes in previous cycle
        {
            if (profileLess) return 10000;
            if (configs.RealTime) return 0;
            var changes = 0;
            foreach (var m in Mirrors)
            {
                changes = m.CriticalChanges++;
            }
            if (changes > 0)
            {
                return 1000;
            }
            return 10000;
        }
        public void Stop()
        {
            if (!_mainStarted) return;
            Console.WriteLine("Stopping all mirrors");
            MainRunning = false;
            _mainStarted = false;
            Console.WriteLine("Waiting for mirrors to complete");
            Thread.Sleep(10000); //timeout for mirrors to complete
            foreach (var m in Mirrors)
            {
                m.tokenSource.Cancel();
            }
            Console.WriteLine("Waiting for mirrors to cancel");
            foreach (var m in Mirrors)
            {
                try
                {
                    m.Run.Wait();
                }
                catch (AggregateException e)
                {
                    Console.WriteLine("AggregateException during service stop");
                }   
            }
            Console.WriteLine("Waiting Main and restart");
            if (MainTask != null) //allows abrupt Stopping
            {
                Task.WaitAll(MainTask);
                MainTask.Dispose();
            }   
            Console.WriteLine("Main disposed"); 
            Mirrors = null;
            System.GC.Collect();
            Console.WriteLine("Mirrors full stop");
        }

        public void Restart()
        {
            Stop();
            Console.WriteLine("Waiting for next run");
            Task.Run(()=>
            {
                Thread.Sleep(10000);
                Start();
            });  
        }
    }
}
